package io.cmdaa.data.migration

import io.cmdaa.data.util.entity._
import slick.jdbc.JdbcBackend.Database
import slick.migration.api.flyway.{MigrationInfo, SlickFlyway, VersionedMigration}
import slick.migration.api.{H2Dialect, Migration, PostgresDialect, TableMigration}

class Version1 (dbProfile: String, test: Boolean = false) {


  implicit val dialect = if( test ) new H2Dialect else new PostgresDialect


  /************************************************************ USER PROJECT SCHEMA ************************************/

  val userTable = TableMigration(UserSchema.users).create
    .addColumns(
      _.id,
      _.username,
      _.passwordHash,
      _.firstName,
      _.lastName,
      _.email,
      _.active,
      _.approved,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val systemTable = TableMigration(UserSchema.systems)
    .create
    .addColumns(
      _.id,
      _.name,
      _.description,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val projectTable = TableMigration(UserSchema.projects)
    .create
    .addColumns(
      _.id,
      _.name,
      _.systemId,
      _.ownerId,
      _.suggestRegEx,
      _.regExQuantity,
      _.regExUnit,
      _.deployStatus,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val projectsDeployedTable = TableMigration(UserSchema.projectsDeployed)
    .create
    .addColumns(
      _.projectId,
      _.deployedType,
      _.deployedBy,
      _.deployedDateMillis
    )

  val projectsRejectedTable = TableMigration(UserSchema.projectsRejected)
    .create
    .addColumns(
      _.projectId,
      _.rejectedReason,
      _.rejectedBy,
      _.rejectedDateMillis
    )

  val rolesTable = TableMigration(UserSchema.roles)
    .create.addColumns(
    _.id,
    _.name,
    _.description,
    _.modifiedBy,
    _.modifiedDateMillis
  )

  val userProjTable = TableMigration(UserSchema.userProjects)
    .create
    .addColumns(
      _.userId,
      _.projectId,
      _.roleId
    )

  val notificationTable = TableMigration(UserSchema.notifications)
    .create
    .addColumns(
      _.id,
      _.message,
      _.severity,
      _.issuedDateMillis
    )

  val userProjectNotTable = TableMigration(UserSchema.userProjectNotifications)
    .create
    .addColumns(
      _.userId,
      _.projectId,
      _.notificationId,
      _.ackDateMillis
    )

  val accessTokenTable = TableMigration(UserSchema.accessTokens)
    .create
    .addColumns(
      _.uuid,
      _.userId,
      _.lastAccessDateMillis
    )

  val userAppPropsTable = TableMigration(UserSchema.userApplicationProperties)
    .create
    .addColumns(
      _.userId,
      _.key,
      _.value
    )

  /************************************************************ USER PROJECT SCHEMA ************************************/

  /************************************************************ SERVER SCHEMA *******************************************/

  val serverTable = TableMigration(ServerSchema.servers)
    .create
    .addColumns(
      _.id,
      _.fqdn,
      _.ipAddrV4,
      _.ipAddrV6,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val labelTable = TableMigration(ServerSchema.labels)
    .create
    .addColumns(
      _.id,
      _.name,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val serverLabelTable = TableMigration(ServerSchema.serverLabels)
    .create
    .addColumns(
      _.serverId,
      _.labelId
    )
  /************************************************************ SERVER SCHEMA *******************************************/

  /*********************************************************** LOG LINE SCHEMA ******************************************/

  val logLineCountsTable = TableMigration(LogLineCountSchema.logLineCounts)
    .create
    .addColumns(
      _.id,
      _.logId,
      _.md5Id,
      _.count
    )
  /*********************************************************** LOG LINE SCHEMA ******************************************/

  /********************************************************** LOG LIKELI SCHEMA *****************************************/
  val logLikeLihoodRunTable = TableMigration(LogLikelihoodSchema.logLikelihoodRun)
    .create
    .addColumns(
      _.id,
      _.cosThreshold,
      _.logLikelihoodThreshold,
      _.runDate,
      _.logFileId
    )
  /********************************************************** LOG LIKELI SCHEMA *****************************************/

  /********************************************************** LOG FILE SCHEMA *****************************************/
  val logSourceTable = TableMigration(LogFileSchema.logSources)
    .create
    .addColumns(
      _.id,
      _.name,
      _.typeId,
      _.notifyRowQty,
      _.meta,
      _.projectId,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val logSourceTypesTable = TableMigration(LogFileSchema.logSourceTypes)
    .create
    .addColumns(
      _.id,
      _.name,
      _.metaDef,
      _.modifiedBy,
      _.modifiedDateMillis
    )

  val logFilesTable = TableMigration(LogFileSchema.logFiles)
    .create
    .addColumns(
      _.id,
      _.name,
      _.logLocation,
      _.owningProjectId
    )

  val logLinesTable = TableMigration(LogFileSchema.logLines)
    .create
    .addColumns(
      _.id,
      _.logLine,
      _.logFileId,
      _.retain
    )
  /********************************************************** LOG FILE SCHEMA *****************************************/

  /************************************************************ GROK SCHEMA *******************************************/
  val grokConfigTable = TableMigration(GrokSchema.grokConfigs)
    .create
    .addColumns(
      _.id,
      _.active,
      _.version,
      _.logSourceId,
      _.modifiedBy,
      _.modifiedDate
    )

  val grokGrokConfigsJoinTable = TableMigration(GrokSchema.grokGrokConfigs)
    .create
    .addColumns(
      _.grokConfigId,
      _.grokId
    )

  val grokTable = TableMigration(GrokSchema.groks)
    .create
    .addColumns(
      _.id,
      _.parentId,
      _.uuid,
      _.active,
      _.grok,
      _.verified,
      _.logSourceId
    )

  val grokLogMessagesTable = TableMigration(GrokSchema.grokLogMessages)
    .create
    .addColumns(
      _.id,
      _.logMessage,
      _.grokId,
      _.logSourceId
    )
  /************************************************************ GROK SCHEMA *******************************************/


  /************************************************************ USER PROJECT SCHEMA ************************************/

  val userUpdate = TableMigration(UserSchema.users)
    .addIndexes(
      _.idxUniqueUsername
    )

  val systemUpdate = TableMigration(UserSchema.systems)
    .addIndexes(
      _.idxUniqueName
    )

  val projectUpdate = TableMigration(UserSchema.projects)
    .addForeignKeys(
      _.system,
      _.owner
    )

  val projectsDeployedUpdate = TableMigration(UserSchema.projectsDeployed)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.fkProject
    )

  val projectsRejectedUpdate = TableMigration(UserSchema.projectsRejected)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.fkProject
    )

  val rolesUpdate = TableMigration(UserSchema.roles)
    .addIndexes(
      _.idxUniqueName
    )

  val userProjUpdate = TableMigration(UserSchema.userProjects)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.user,
      _.project,
      _.role
    )

  val userProjectNotUpdate = TableMigration(UserSchema.userProjectNotifications)
    .addForeignKeys(
      _.fkUser,
      _.fkProject,
      _.fkNotification
    )

  val accessTokenUpdate = TableMigration(UserSchema.accessTokens)
    .addForeignKeys(
      _.user
    )

  val userAppPropsUpdate = TableMigration(UserSchema.userApplicationProperties)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.fkUser
    )

  /************************************************************ USER PROJECT SCHEMA ************************************/

  /************************************************************ SERVER SCHEMA *******************************************/
  val serverUpdate = TableMigration(ServerSchema.servers)
    .addIndexes(
      _.idxUniqueFqdn
    )

  val labelUpdate = TableMigration(ServerSchema.labels)
    .addIndexes(
      _.idxUniqueName
    )

  val serverLabelUpdate = TableMigration(ServerSchema.serverLabels)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.server,
      _.label
    )
  /************************************************************ SERVER SCHEMA *******************************************/

  /*********************************************************** LOG LINE SCHEMA ******************************************/
  val logLineCountsUpdate = TableMigration(LogLineCountSchema.logLineCounts)
    .addIndexes(
      _.idx
    )
    .addForeignKeys(
      _.log
    )
  /*********************************************************** LOG LINE SCHEMA ******************************************/

  /********************************************************** LOG LIKELI SCHEMA *****************************************/
  val logLikeLihoodRunUpdate = TableMigration(LogLikelihoodSchema.logLikelihoodRun)
    .addForeignKeys(
      _.logFile
    )
  /********************************************************** LOG LIKELI SCHEMA *****************************************/

  /********************************************************** LOG FILE SCHEMA *****************************************/
  val logSourceUpdate = TableMigration(LogFileSchema.logSources)
    .addIndexes(
      _.idxUniqueLogSource
    )
    .addForeignKeys(
      _.project,
      _.logSourceType
    )

  val logSourceTypesUpdate = TableMigration(LogFileSchema.logSourceTypes)
    .addIndexes(
      _.idxUniqueLogSourceType
    )

  val logFilesUpdate = TableMigration(LogFileSchema.logFiles)
    .addForeignKeys(
      _.owningProject
    )

  val logLinesUpdate = TableMigration(LogFileSchema.logLines)
    .addForeignKeys(
      _.logFile
    )
  /********************************************************** LOG FILE SCHEMA *****************************************/

  /************************************************************ GROK SCHEMA *******************************************/
  //  userUpdate &
  //  systemUpdate &
  //  projectUpdate &
  //  projectsDeployedUpdate &
  //  projectsRejectedUpdate &
  //  rolesUpdate &
  //  userProjUpdate &
  //  userProjectNotUpdate &
  //  accessTokenUpdate &
  //  userAppPropsUpdate &

  //  serverUpdate &
  //  labelUpdate &
  //  serverLabelUpdate &

  //  logLineCountsUpdate &

  //  logLikeLihoodRunUpdate &

  //  logSourceUpdate &
  //  logSourceTypesUpdate &
  //  logFilesUpdate &
  //  logLinesUpdate &

  //  grokGrokConfigsJoinUpdate &
  //  grokLogMessagesUpdate &


  val grokGrokConfigsJoinUpdate = TableMigration(GrokSchema.grokGrokConfigs)
    .addPrimaryKeys(
      _.pk
    )
    .addForeignKeys(
      _.grokConfg,
      _.grok
    )

  val grokLogMessagesUpdate = TableMigration(GrokSchema.grokLogMessages)
    .addForeignKeys(
      _.grok,
      _.logSource
    )
  /************************************************************ GROK SCHEMA *******************************************/


  def runMigration():Unit = {
    val db = Database.forConfig(dbProfile)

//    val v1 = new Version1(true)

    implicit val infoProvider: MigrationInfo.Provider[Migration] = MigrationInfo.Provider.strict

    //create tables and then add primary key and foreign key relationships
    val migration = VersionedMigration(
      "1",
      userTable &
      systemTable &
      projectTable &
      projectsDeployedTable &
      projectsRejectedTable &
      rolesTable &
      userProjTable &
      notificationTable &
      userProjectNotTable &
      accessTokenTable &
      userAppPropsTable &
      serverTable &
      labelTable &
      serverLabelTable &
      logLineCountsTable &
      logLikeLihoodRunTable &
      logSourceTable &
      logSourceTypesTable &
      logFilesTable &
      logLinesTable &
      grokConfigTable &
      grokGrokConfigsJoinTable &
      grokTable &
      grokLogMessagesTable &

      userUpdate &
      systemUpdate &
      projectUpdate &
      projectsDeployedUpdate &
      projectsRejectedUpdate &
      rolesUpdate &
      userProjUpdate &
      userProjectNotUpdate &
      accessTokenUpdate &
      userAppPropsUpdate &
      serverUpdate &
      labelUpdate &
      serverLabelUpdate &
      logLineCountsUpdate &
      logLikeLihoodRunUpdate &
      logSourceUpdate &
      logSourceTypesUpdate &
      logFilesUpdate &
      logLinesUpdate &
      grokGrokConfigsJoinUpdate &
      grokLogMessagesUpdate
    )

    val createTable =
      SlickFlyway(db)(Seq(migration))
        .load()

    createTable.migrate()
  }
}

