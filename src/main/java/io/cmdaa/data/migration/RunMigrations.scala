package io.cmdaa.data.migration

import org.rogach.scallop.ScallopConf
import org.rogach.scallop.exceptions.ScallopException


class LogLikelihoodConf( arguments: Seq[String] ) extends ScallopConf( arguments ) {
  val version = opt[String](required = true)
  val dbType = opt[String](required = true)

  /**
    * Overridden implementation throws an uncaught exception in all cases
    */
  override def onError(e: Throwable): Unit = e match {
    case ScallopException(message) => throw new IllegalStateException(e)

    case e => throw new RuntimeException("Non-CLI exception: ", e)
  }
}

/**
  * dbType = postgresql
 */
object RunMigrations {

  val V_1 = "v1"
  val ALL = "all"
  val NONE = "none"

  def runMigrations(args: Array[String]): Unit = {
    val conf = new LogLikelihoodConf(args)
    var myVar = "theValue";

    conf.version.apply() match {
      case V_1 => {
        new Version1( conf.dbType.apply() ).runMigration()
      }

      case ALL => {
        new Version1( conf.dbType.apply() ).runMigration()
      }

      case default => println("Invalid Version selected")
    }
  }

  def main(args: Array[String]): Unit = {
    try {
      runMigrations(args)
    } catch {
     case e: Exception => sys.exit(1)
    }

    sys.exit(0)
  }
}