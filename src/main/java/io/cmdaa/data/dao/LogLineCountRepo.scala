package io.cmdaa.data.dao

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.LogLineCountSchema
import io.cmdaa.data.util.entity.LogLineCountSchema.LogLineCount
import slick.jdbc.JdbcBackend.Database
//import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.language.higherKinds

/** This repository class will handle all log line count transactions.
  *
  * @param profile   the injectible data base profile that will be used.<br>
  *                  MySqlProfile,<br>
  *                  H2Profile, <br>
  *                  etc...<br>
  * @param db        the injectible database object that will be used.<br>
  * @see io.cmdaa.data.dao.LogLineRepoTest
  */
class LogLineCountRepo(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action.transactionally), Duration.Inf)

  /** This method will insert set of log line counts for a specific log file.  The log line  must be associated with a log file in the system.
    *
    * @param lineCounts
    *
    */

  def insert(lineCounts: LogLineCount): Unit = {
    val session = db.createSession()

    try {
      exec( LogLineCountSchema.logLineCounts += lineCounts )
    } finally {
      session.close()
    }
  }

  def insert(lineCounts: Stream[LogLineCount]): Unit = {
    val session = db.createSession()

    try {
      exec( LogLineCountSchema.logLineCounts ++= lineCounts )
    } finally {
      session.close()
    }
  }

  def deleteByLogId(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( LogLineCountSchema.logLineCounts.filter(_.logId === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will return all the log line counts that are associated
    * with a log file.
    *
    * @param id the log file associated with log lines
    * @return the log lines returned
    */
  def findAllLinesByLogFile(id: Long): Stream[LogLineCount] = {
    val session = db.createSession()

    try {
      exec( LogLineCountSchema.logLineCounts.filter(_.logId === id).result ).toStream
    } finally {
      session.close()
    }
  }

  def sendTransaction(statementList: Seq[String]): Seq[Int] = {
    val session = db.createSession()

    try {

      //Add transaction statements to list
      //   def createTransSeq(statementSeq: Seq[String]) = "start transaction;  " +: statementSeq :+ "commit; "

      def createTransSeq(statementSeq: Seq[String]) = statementSeq

      //Create fct to transform Strings to SQL statements for Slick
      def createSqlAction(line: String): DBIO[Int] = sqlu"#$line"

      val transSeq = createTransSeq(statementList)

      //Apply the createSqlAction fct via the map method
      val actionSeq: Seq[DBIO[Int]] = transSeq.map(createSqlAction)

      //Format the Seq of Actions to DBIO Sequence
      val runSeq: DBIO[Seq[Int]] = DBIO.sequence(actionSeq)

      //Send the actions to the database and wait for the response
      //Response will be a Seq of Int that indicate how many updates or inserts were completed for each
      //statement.  For the "Start Transaction" and the "commit" statements there will be a 0.
      Await.result(db.run(runSeq.transactionally), Duration.Inf)

    } finally {
      session.close()
    }
  }

}