package io.cmdaa.data.dao

import java.util.{Calendar, UUID}

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.LogFileSchema.{LogSource, _}
import io.cmdaa.data.util.entity.UserSchema
import io.cmdaa.data.util.entity.UserSchema._
import slick.jdbc.JdbcBackend.Database
//import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/** This class will handle all interactions for projects and users of the
  * system.  The schema is designed to mimic the functionality of git hub
  * and git lab projects.
  *
  * @param profile   the injectible data base profile that will be used.<br>
  *                  MySqlProfile,<br>
  *                  H2Profile, <br>
  *                  etc...<br>
  * @param db        the injectible database object that will be used.<br>
  * @see io.cmdaa.data.dao.LogLineRepoTest
  */
class UserProjectRepo(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action.transactionally), Duration.Inf)

  /**
    * Insert a new user
    *
    * @param user User
    * @return User with generated identifier
    */
  def insertUser(user: User): User = {
    val session = db.createSession()

    try {
      exec( users returning users.map(_.id) into ((user, id) => user.copy(id = Some(id))) += user )
     } finally {
      session.close()
    }
  }

  /**
    * Update an existing user
    *
    * @param user User
    * @return The number of users updated
    */
  def updateUser(user: User): Int = {
    val session = db.createSession()

    try {
      exec(
        users.filter(_.id === user.id)
          .map(u => (u.username, u.firstName, u.lastName,
            u.email, u.active, u.approved, u.modifiedBy, u.modifiedDateMillis))
          .update((user.username, user.firstName, user.lastName,
            user.email, user.active, user.approved, user.modifiedBy, user.modifiedDateMillis))
      )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing user's password hash
    *
    * @param userId       User ID
    * @param passwordHash The salted password hash
    * @return The number of users updated
    */
  def updatePassword(userId: Long, passwordHash: String): Int = {
    val session = db.createSession()

    try {
      exec( ( for {u <- users if u.id === userId} yield u.passwordHash).update((passwordHash) ) )
    } finally {
      session.close()
    }
  }

  /**
    * Delete an existing user
    *
    * @param id User Id
    * @return The number of users deleted
    */
  def deleteUser(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( users.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will return a user that has been specified within
    * the query parameters.
    *
    * @param id User Id
    * @return User
    */
  def findUser(id: Long): Option[User] = {
    val session = db.createSession()

    try {
      exec( users.filter(_.id === id).result ).headOption
     } finally {
      session.close()
    }
  }

  /**
    * Find a user by username
    *
    * @param username Username
    * @return The first user found or none
    */
  def findUserByUsername(username: String): Option[User] = {
    val session = db.createSession()

    try {
      exec( users.filter(_.username === username).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Return a list of all users
    *
    * @return a list of users
    */
  def listUsers(): List[User] = {
    val session = db.createSession()

    try {
      exec( users.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Insert a new role
    *
    * @param role Role
    */
  def insertRole(role: Role): Unit = {
    val session = db.createSession()

    try {
      exec( roles += role )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing role
    *
    * @param role Role
    * @return The number of roles updated
    */
  def updateRole(role: Role): Int = {
    val session = db.createSession()

    try {
      exec(
        roles.filter(_.id === role.id)
          .map(r => (r.name, r.description, r.modifiedBy, r.modifiedDateMillis))
          .update((role.name, role.description, role.modifiedBy, role.modifiedDateMillis))
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete an existing role
    *
    * @param id Role Id
    * @return The number of roles deleted
    */
  def deleteRole(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( roles.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Find a role by its id
    *
    * @param id Role Id
    * @return Role
    */
  def findRole(id: Long): Option[Role] = {
    val session = db.createSession()

    try {
      exec( roles.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find all roles
    *
    * @return List of all roles
    */
  def listRoles(): List[Role] = {
    val session = db.createSession()

    try {
      exec( roles.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Insert a new system
    *
    * @param system System
    */
  def insertSystem(system: UserSchema.System): Unit = {
    val session = db.createSession()

    try {
      exec( systems += system )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing system
    *
    * @param system System
    * @return The number of systems updated
    */
  def updateSystem(system: UserSchema.System): Int = {
    val session = db.createSession()

    try {
      exec(
        systems.filter(_.id === system.id)
        .map(s => (s.name, s.description, s.modifiedBy, s.modifiedDateMillis))
        .update((system.name, system.description, system.modifiedBy, system.modifiedDateMillis))
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete an existing system
    *
    * @param id System Id
    * @return The number of systems deleted
    */
  def deleteSystem(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( systems.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Find a system by its id
    *
    * @param id System Id
    * @return System
    */
  def findSystem(id: Long): Option[UserSchema.System] = {
    val session = db.createSession()

    try {
      exec( systems.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find all systems
    *
    * @return List of all systems
    */
  def listSystems(): List[UserSchema.System] = {
    val session = db.createSession()

    try {
      exec( systems.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Insert a new Project
    *
    * @param project Project
    */
  def insertProject(project: Project): Project = {
    val session = db.createSession()

    try {
      exec( projects returning projects.map(_.id) into ((project, id) => project.copy(id = Some(id))) += project)
     } finally {
      session.close()
    }
  }

  /**
    * Insert a Project with related users and roles
    *
    * @param project         Project to insert
    * @param logSourceTuples List of log source tuples
    * @param userRoleTuples  List of user and role identifier tuples
    * @return The project with autogenerated identifier
    */
  def insertProject(project: Project, logSourceTuples: List[(Option[Long], String, Long, Option[Long], String, Option[Long], String, Long)], userRoleTuples: List[(Long, Long)]): Project = {
    val session = db.createSession()

    try {

      val genUserRoles = (project: Project, userRoleTuples: List[(Long, Long)]) => {
        val l = new ListBuffer[UserProject]()
        for (tuple <- userRoleTuples) l += UserProject(tuple._1, project.id.get, tuple._2)
        l.toList
      }

      val genLogSources = (project: Project, logSourceTuples: List[(Option[Long], String, Long, Option[Long], String, Option[Long], String, Long)]) => {
        val l = new ListBuffer[LogSource]()
        for (tuple <- logSourceTuples) l += LogSource(tuple._1, tuple._2, tuple._3, tuple._4, tuple._5, project.id.get, tuple._7, tuple._8)
        l.toList
      }

      val actions = for {
        rs <- projects returning projects.map(_.id) into ((project, id) => project.copy(id = Some(id))) += project
        _ <- DBIO.seq(userProjects ++= genUserRoles(rs, userRoleTuples))
        _ <- DBIO.seq(logSources ++= genLogSources(rs, logSourceTuples))
      } yield rs

      exec( actions )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing project
    *
    * @param project Project
    * @return The number of projects updated
    */
  def updateProject(project: Project): Int = {
    val session = db.createSession()

    try {
      exec(
        projects.filter(_.id === project.id)
        .map(p => (p.name, p.systemId, p.ownerId, p.suggestRegEx, p.regExQuantity, p.regExUnit, p.deployStatus, p.modifiedBy, p.modifiedDateMillis))
        .update((project.name, project.systemId, project.ownerId, project.suggestRegEx, project.regExQuantity, project.regExUnit, project.deployStatus, project.modifiedBy, project.modifiedDateMillis))
      )

    } finally {
      session.close()
    }
  }

  /**
    * Update an existing project with related users and roles
    *
    * @param project        Project to update
    * @param userRoleTuples List of user and role identifier tuples
    * @return The number of projects updated
    */
  def updateProject(project: Project, userRoleTuples: List[(Long, Long)]): Int = {
    val session = db.createSession()

    try {

      val gen = (project: Project, userRoleTuples: List[(Long, Long)]) => {
        val l = new ListBuffer[UserProject]()
        for (tuple <- userRoleTuples) l += UserProject(tuple._1, project.id.get, tuple._2)
        l.toList
      }

      exec(
        for {
          rs <- projects.filter(_.id === project.id)
            .map(p => (p.name, p.systemId, p.ownerId, p.suggestRegEx, p.regExQuantity, p.regExUnit, p.deployStatus, p.modifiedBy, p.modifiedDateMillis))
            .update((project.name, project.systemId, project.ownerId, project.suggestRegEx, project.regExQuantity, project.regExUnit, project.deployStatus, project.modifiedBy, project.modifiedDateMillis))
          _ <- userProjects.filter(x => x.projectId === project.id).delete
          _ <- DBIO.seq(userProjects ++= gen(project, userRoleTuples))
        } yield rs
      )
    } finally {
      session.close()
    }
  }

  /**
    * Deploy a project (TEST or PRODUCTION)
    *
    * @param deployment Project deployment
    */
  def deployProject(deployment: ProjectDeployed): Unit = {
    val session = db.createSession()

    try {
      exec(
        for {
          insert <- DBIO.seq(projectsDeployed += deployment)
          _ <- projects.filter(_.id === deployment.projectId).map(p => p.deployStatus).update("DEPLOYED")
        } yield insert
      )
    } finally {
      session.close()
    }
  }

  /**
    *
    * @param projectId Project identifier
    * @return List of deployments for specified project
    */
  def listDeploymentsForProject(projectId: Long): List[ProjectDeployed] = {
    val session = db.createSession()

    try {
      exec( projectsDeployed.filter(_.projectId === projectId).sortBy(_.projectId.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Reject a project
    *
    * @param rejection Project rejection
    */
  def rejectProject(rejection: ProjectRejected): Unit = {
    val session = db.createSession()

    try {
     exec(
       for {
         insert <- DBIO.seq(projectsRejected += rejection)
         _ <- projects.filter(_.id === rejection.projectId).map(p => p.deployStatus).update("REJECTED")
       } yield insert
     )
    } finally {
      session.close()
    }
  }

  /**
    *
    * @param projectId Project identifier
    * @return List of rejections for specified project
    */
  def listRejectionsForProject(projectId: Long): List[ProjectRejected] = {
    val session = db.createSession()

    try {
      exec( projectsRejected.filter(_.projectId === projectId).sortBy(_.projectId.asc).result ).toList
     } finally {
      session.close()
    }
  }

  /**
    * Delete an existing project
    *
    * @param id Project identifier
    * @return The number of projects deleted
    */
  def deleteProject(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( projects.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Find a project by its id
    *
    * @param id Project identifier
    * @return Project
    */
  def findProject(id: Long): Option[Project] = {
    val session = db.createSession()

    try {
      exec( projects.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find a project for a specific user
    *
    * @param id     Project identifier
    * @param userId User identifier
    * @return (Project, UserProject) tuple
    */
  def findProjectForUser(id: Long, userId: Long): Option[(Project, UserProject)] = {
    val session = db.createSession()

    try {
      val innerJoin = for {
        (p, u) <- projects join userProjects on (_.id === _.projectId)
      } yield (p, u)

      exec( innerJoin.filter(x => x._2.projectId === id && x._2.userId === userId).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find a project for a specified owner
    *
    * @param id     Project identifier
    * @param userId Owner's user identifier
    * @return Project
    */
  def findProjectForOwner(id: Long, userId: Long): Option[Project] = {
    val session = db.createSession()

    try {
      exec( projects.filter(x => x.id === id && x.ownerId === userId).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find all projects
    *
    * @return List of all projects
    */
  def listProjects(): List[Project] = {
    val session = db.createSession()

    try {
      val query = projects.sortBy(_.id.asc).result
      val future: Future[Seq[Project]] = db.run(query)
      val messagesResults = Await.result(future, Duration.Inf)
      messagesResults.toList
    } finally {
      session.close()
    }
  }

  /**
    * Find all projects for a user
    *
    * @param userId User identifier
    * @return List of all (Project, UserProject) tuples for specified user identifier
    */
  def listProjectsForUser(userId: Long): List[(Project, UserProject)] = {
    val session = db.createSession()

    try {
      val innerJoin = for {
        (p, u) <- projects join userProjects on (_.id === _.projectId)
      } yield (p, u)
      exec( innerJoin.filter(_._2.userId === userId).sortBy(_._1.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Find all projects for an owner
    *
    * @param userId User identifier for project owner
    * @return List of all Projects owned by specified user
    */
  def listProjectsForOwner(userId: Long): List[Project] = {
    val session = db.createSession()

    try {
      exec( projects.filter(_.ownerId === userId).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Return a list of all log sources belonging to the specified project
    *
    * @param projectId Project identifier
    * @return List of log sources
    */
  def listLogSourcesForProject(projectId: Long): List[LogSource] = {
    val session = db.createSession()

    try {
      exec( logSources.filter(_.projectId === projectId).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * List all users associated with specified project
    *
    * @param projectId Project identifier
    * @return list of users associated with project
    */
  def listUsersForProject(projectId: Long): List[User] = {
    val session = db.createSession()

    try {
      exec( users.filter(_.id in userProjects.filter(_.projectId === projectId).map(_.userId)).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * List all users with roles associated with specified project
    *
    * @param projectId Project identifier
    * @return list of users/roles associated with project
    */
  def listUserRolesForProject(projectId: Long): List[(User, Role)] = {
    val session = db.createSession()

    try {
      val innerJoin = for {
        ((u, up), r) <- users join userProjects on (_.id === _.userId) join roles on (_._2.roleId === _.id)
        if up.projectId === projectId
      } yield (u, r)
      exec( innerJoin.sortBy(_._1.id.asc).result ).toList
     } finally {
      session.close()
    }
  }

  /**
    * Get the system associated with specified project
    *
    * @param projectId Project identifier
    * @return system associated with project
    */
  def getSystemForProject(projectId: Long): Option[UserSchema.System] = {
    val session = db.createSession()

    try {
      exec( systems.filter(_.id in projects.filter(_.id === projectId).map(_.systemId)).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Get the owner of the specified project
    *
    * @param projectId Project identifier
    * @return User
    */
  def getOwnerForProject(projectId: Long): Option[User] = {
    val session = db.createSession()

    try {
      exec( users.filter(_.id in projects.filter(_.id === projectId).map(_.ownerId)).result ).headOption
     } finally {
      session.close()
    }
  }

  /**
    * Insert a new Notification
    *
    * @param notification Notification
    */
  def insertNotification(notification: Notification): Notification = {
    val session = db.createSession()

    try {
      exec( notifications returning notifications.map(_.id) into ((notification, id) => notification.copy(id = Some(id))) += notification )
    } finally {
      session.close()
    }
  }

  /**
    * Insert a notification with related users and/or projects
    *
    * @param notification      Notification to insert
    * @param userProjectTuples List of user and project identifier tuples
    * @return The notification with autogenerated identifier
    */
  def insertNotification(notification: Notification, userProjectTuples: List[(Option[Long], Option[Long])]): Notification = {
    val session = db.createSession()

    try {

      val gen = (notification: Notification, userProjectTuples: List[(Option[Long], Option[Long])]) => {
        val l = new ListBuffer[UserProjectNotification]()
        for (tuple <- userProjectTuples) l += UserProjectNotification(tuple._1, tuple._2, notification.id.get)
        l.toList
      }

      exec(
        for {
          rs <- notifications returning notifications.map(_.id) into ((notification, id) => notification.copy(id = Some(id))) += notification
          _ <- DBIO.seq(userProjectNotifications ++= gen(rs, userProjectTuples))
        } yield rs
      )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing notification
    *
    * @param notification Notification
    * @return The number of notifications updated
    */
  def updateNotification(notification: Notification): Int = {
    val session = db.createSession()

    try {
      exec(
        notifications.filter(_.id === notification.id)
        .map(n => (n.message, n.severity, n.issuedDateMillis))
        .update((notification.message, notification.severity, notification.issuedDateMillis))
      )
    } finally {
      session.close()
    }
  }

  /**
    * Update an existing notification with related users and/or projects
    *
    * @param notification      Notification to update
    * @param userProjectTuples List of user and project identifier tuples
    * @return The number of notifications updated
    */
  def updateNotification(notification: Notification, userProjectTuples: List[(Option[Long], Option[Long])]): Int = {
    val session = db.createSession()

    try {
      val gen = (notification: Notification, userProjectTuples: List[(Option[Long], Option[Long])]) => {
        val l = new ListBuffer[UserProjectNotification]()
        for (tuple <- userProjectTuples) l += UserProjectNotification(tuple._1, tuple._2, notification.id.get)
        l.toList
      }

      exec(
        for {
          rs <- notifications.filter(_.id === notification.id)
            .map(n => (n.message, n.severity, n.issuedDateMillis))
            .update((notification.message, notification.severity, notification.issuedDateMillis))
          _ <- userProjectNotifications.filter(x => x.notificationId === notification.id).delete
          _ <- DBIO.seq(userProjectNotifications ++= gen(notification, userProjectTuples))
        } yield rs
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete an existing notification
    *
    * @param id Notification identifier
    * @return The number of systems deleted
    */
  def deleteNotification(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( notifications.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Find a notification by its id
    *
    * @param id Notification identifier
    * @return Notification
    */
  def findNotification(id: Long): Option[Notification] = {
    val session = db.createSession()

    try {
      exec( notifications.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Find a notification for a specific user
    *
    * @param id     Notification identifier
    * @param userId User identifier
    * @return (Notification, UserProjectNotification) tuple
    */
  def findNotificationForUser(id: Long, userId: Long): Option[(Notification, UserProjectNotification)] = {
    val session = db.createSession()

    try {
      val innerJoin = for {
        (n, u) <- notifications join userProjectNotifications on (_.id === _.notificationId)
      } yield (n, u)
      exec( innerJoin.filter(x => x._2.notificationId === id && x._2.userId === userId).result ).headOption
     } finally {
      session.close()
    }
  }

  /**
    * Return a paginated list of all notifications
    *
    * @param pagination Optional pagination (offset, limit) tuple
    * @return (Total count of notifications, List of notifications) tuple
    */
  def listNotifications(pagination: Option[(Long, Long)] = None): (Int, List[Notification]) = {
    val session = db.createSession()

    try {
      var page = notifications.sortBy(_.id.asc)
      if (pagination.nonEmpty) {
        page = page.drop(pagination.get._1).take(pagination.get._2)
      }

      val results = exec(
        for {
          count <- notifications.length.result
          resp <- page.result
        } yield (count, resp)
      )
      (results._1, results._2.toList)
    } finally {
      session.close()
    }
  }

  /**
    * Return a paginated list of notifications for a user
    *
    * @param userId     User identifier
    * @param pagination Optional pagination (offset, limit) tuple
    * @return (Total count of notifications for user, List of all (Notification, UserProjectNotification) tuples for user) tuple
    */
  def listNotificationsForUser(userId: Long, pagination: Option[(Long, Long)] = None): (Int, List[(Notification, UserProjectNotification)]) = {
    val session = db.createSession()

    try {

      val innerJoin = for {
        (n, u) <- notifications join userProjectNotifications on (_.id === _.notificationId)
      } yield (n, u)
      val query = innerJoin.filter(_._2.userId === userId)
      var page = query.sortBy(_._1.id.asc)
      if (pagination.nonEmpty) {
        page = page.drop(pagination.get._1).take(pagination.get._2)
      }

      val results = exec(
        for {
          count <- query.length.result
          resp <- page.result
        } yield (count, resp)
      )

      (results._1, results._2.toList)
    } finally {
      session.close()
    }
  }

  /**
    * Find all notifications for a project
    *
    * @param projectId Project identifier
    * @return List of all notifications for specified project identifier
    */

  def listNotificationsForProject(projectId: Long): List[Notification] = {
    val session = db.createSession()

    try {
      exec( notifications.filter(_.id in userProjectNotifications.filter(_.projectId === projectId).map(_.notificationId)).sortBy(_.id.asc).result ).toList
     } finally {
      session.close()
    }
  }

  /**
    * Find all notifications for a user and project
    *
    * @param userId    User identifier
    * @param projectId Project identifier
    * @return List of all notifications for specified user and project identifiers
    */
  def listNotificationsForUserAndProject(userId: Long, projectId: Long): List[Notification] = {
    val session = db.createSession()

    try {
      exec( notifications.filter(_.id in userProjectNotifications.filter(x => x.userId === userId && x.projectId === projectId).map(_.notificationId)).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Return the count of unacknowledged notifications for a specified user
    *
    * @param userId User identifier
    * @return The count of all unacknowledged notifications for specified user
    */
  def countUnacknowledgedNotificationsForUser(userId: Long): Int = {
    val session = db.createSession()

    try {
      exec(  notifications.filter(_.id in userProjectNotifications.filter(x => x.userId === userId && x.ackDateMillis.isEmpty).map(_.notificationId)).length.result )
    } finally {
      session.close()
    }
  }

  /**
    * Acknowledge a notification
    *
    * @param userId         User identifier
    * @param notificationId Notification identifier
    * @param ackDateMillis  Acknowledgement date (in milliseconds since epoch)
    * @return Number of records updated
    */
  def acknowledgeNotification(userId: Long, notificationId: Long, ackDateMillis: Long): Int = {
    val session = db.createSession()

    try {
      exec(
        userProjectNotifications.filter(x => x.userId === userId && x.notificationId === notificationId)
        .map(n => n.ackDateMillis)
        .update(Some(ackDateMillis))
      )
     } finally {
      session.close()
    }
  }

  /**
    * Acknowledge a notification
    *
    * @param userId         User identifier
    * @param projectId      Project identifier
    * @param notificationId Notification identifier
    * @param ackDateMillis  Acknowledgement date (in milliseconds since epoch)
    * @return Number of records updated
    */
  def acknowledgeNotification(userId: Long, projectId: Long, notificationId: Long, ackDateMillis: Long): Int = {
    val session = db.createSession()

    try {
      exec(
        userProjectNotifications.filter(x => x.userId === userId && x.projectId === projectId && x.notificationId === notificationId)
        .map(n => n.ackDateMillis)
        .update(Some(ackDateMillis))
      )
    } finally {
      session.close()
    }
  }

  /**
    * List all notification acknowledgements for a user
    *
    * @param userId         User identifier
    * @param notificationId Notification identifier
    * @return List of user project notifications
    */
  def listNotificationsAcksForUser(userId: Long, notificationId: Long): List[UserProjectNotification] = {
    val session = db.createSession()

    try {
      exec( userProjectNotifications.filter(x => x.userId === userId && x.notificationId === notificationId).sortBy(_.notificationId.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * List all notification acknowledgements for a user and project
    *
    * @param userId         User identifier
    * @param projectId      Project identifier
    * @param notificationId Notification identifier
    * @return List of user project notifications
    */
  def listNotificationAcksForUserAndProject(userId: Long, projectId: Long, notificationId: Long): List[UserProjectNotification] = {
    val session = db.createSession()

    try {
      exec( userProjectNotifications.filter(x => x.userId === userId && x.projectId === projectId && x.notificationId === notificationId).sortBy(_.projectId.asc).result ).toList
    }
    finally {
      session.close()
    }
  }

  /**
    * List all access tokens
    *
    * @return List of all access tokens
    */
  def listAccessTokens(): List[AccessToken] = {
    val session = db.createSession()

    try {
      exec( accessTokens.sortBy(_.lastAccessDateMillis.desc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Find an access token
    *
    * @param uuid Access token identifier
    * @return The access token
    */
  def findAccessToken(uuid: String): Option[AccessToken] = {
    val session = db.createSession()

    try {
      exec( accessTokens.filter(_.uuid === uuid).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Generate a new access token
    *
    * @param userId User identifier
    * @return Access token
    */
  def generateAccessToken(userId: Long): AccessToken = {
    val session = db.createSession()

    try {
      val token: AccessToken = AccessToken(Some(UUID.randomUUID().toString), userId, Calendar.getInstance().getTime.getTime)
      exec( accessTokens += token )
      token
    } finally {
      session.close()
    }
  }

  /**
    * Update the last accessed date for an access token
    *
    * @param uuid Access token identifier
    * @return The number of records updated
    */
  def touchAccessToken(uuid: String): Int = {
    val session = db.createSession()

    try {
      exec(
        accessTokens.filter(_.uuid === uuid)
        .map(t => t.lastAccessDateMillis)
        .update(Calendar.getInstance().getTime.getTime)
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete an access token
    *
    * @param uuid Access token identifier
    * @return The number of records deleted
    */
  def deleteAccessToken(uuid: String): Int = {
    val session = db.createSession()

    try {
      exec(
        accessTokens.filter(_.uuid === uuid).delete
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete all access tokens for specified user
    *
    * @param userId User identifier
    * @return The number of records deleted
    */
  def deleteAccessTokensForUser(userId: Long): Int = {
    val session = db.createSession()

    try {
      exec( accessTokens.filter(_.userId === userId).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Return the list of all user application properties
    *
    * @return List of user application properties
    */
  def listUserApplicationProperties(): List[UserApplicationProperty] = {
    val session = db.createSession()

    try {
      exec( userApplicationProperties.result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Find a user application property
    *
    * @param userId User identifier
    * @param key    Property key
    * @return The user appliction property
    */
  def findUserApplicationProperty(userId: Long, key: String): Option[UserApplicationProperty] = {
    val session = db.createSession()

    try {
      exec( userApplicationProperties.filter(x => x.userId === userId && x.key === key).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Insert a new user application property
    *
    * @param prop User application property
    */
  def insertUserApplicationProperty(prop: UserApplicationProperty): Unit = {
    val session = db.createSession()

    try {
      exec( userApplicationProperties += prop )
     } finally {
      session.close()
    }
  }

  /**
    * Update a user application property
    *
    * @param prop User application property
    * @return The number of user application properties updated
    */
  def updateUserApplicationProperty(prop: UserApplicationProperty): Int = {
    val session = db.createSession()

    try {
      exec(
        userApplicationProperties.filter(x => x.userId === prop.userId && x.key === prop.key)
        .map(p => p.value)
        .update(prop.value)
      )
    } finally {
      session.close()
    }
  }

  /**
    * Delete a user application property
    *
    * @param userId User identifier
    * @param key    Property key
    * @return The number of user application properties deleted
    */
  def deleteUserApplicationProperty(userId: Long, key: String): Int = {
    val session = db.createSession()

    try {
      exec( userApplicationProperties.filter(x => x.userId === userId && x.key === key).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Return the list of all user application properties for the specified user
    *
    * @param userId User identifier
    * @return List of user application properties
    */
  def listUserApplicationPropertiesForUser(userId: Long): List[UserApplicationProperty] = {
    val session = db.createSession()

    try {
      exec( userApplicationProperties.filter(_.userId === userId).result ).toList
    } finally {
      session.close()
    }
  }

  /** This will return a list of all projects that have been defined
    * within the system.
    *
    * @return a list of all the projects
    */
  def findAllProjects(): List[Project] = {
    val session = db.createSession()

    try {
      exec( projects.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** Find all projects/users/roles defined in the system.
    *
    * @return user project and roles
    */
  def findAllUserProjects(): List[UserProject] = {
    val session = db.createSession()

    try {
      exec( userProjects.sortBy(_.projectId.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** THis method will assign a user and a role with a project.
    *
    * @param userId    User Id
    * @param projectId Project Id
    * @param roleId    Role Id
    */
  def addUserToProject(userId: Long, projectId: Long, roleId: Long): Unit = {
    val session = db.createSession()

    try {
      exec( userProjects += UserProject(userId, projectId, roleId) )
    } finally {
      session.close()
    }
  }

  /** This method will assign a user and a role with a project.
    *
    * @param user    User
    * @param project Project
    * @param role    Role
    */
  def addUserToProject(user: User, project: Project, role: Role): Unit = {
    addUserToProject(user.id.get, project.id.get, role.id.get)
  }

  /** This method will pull back all projects that are tied to the
    * specified user.
    *
    * @param userId the user to query
    * @return a list of projects associated with this user
    */
  def findProjectsByUser(userId: Long): List[Project] = {
    val session = db.createSession()

    try {
      exec( projects.filter(_.id in userProjects.filter(_.userId === userId).map(_.projectId)).sortBy(_.id.asc).result ).toList
     } finally {
      session.close()
    }
  }

  /** This project will pull back all users that are associated with the
    * specified project.
    *
    * @param projectId the project to query
    * @return users that are associated with this project
    */
  def findUsersInProject(projectId: Long): List[User] = {
    val session = db.createSession()

    try {
      exec( users.filter(_.id in userProjects.filter(_.projectId === projectId).map(_.userId)).sortBy(_.id.asc).result ).toList
     } finally {
      session.close()
    }
  }

  /** Pull back all users and usertypes that are associated with a specific project.
    *
    * @param projectId the project to query
    * @return a list of tuples containing Users and Roles that are associate with
    *         this project.
    */
  def getListOfUsersWithUserType(projectId: Long): List[(User, Role)] = {
    val session = db.createSession()

    try {
      exec(Compiled(for {
        gu <- userProjects if gu.projectId === projectId
        u <- users if u.id === gu.userId
        t <- roles if t.id === gu.roleId
      } yield (u, t)).result
      ).toList
    } finally {
      session.close()
    }
  }
}