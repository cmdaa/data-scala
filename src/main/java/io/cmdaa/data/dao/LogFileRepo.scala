package io.cmdaa.data.dao

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.LogFileSchema
import io.cmdaa.data.util.entity.LogFileSchema.{LogFile, LogSource, LogSourceType, _}
import slick.jdbc.JdbcBackend.Database
//import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.language.higherKinds

/** This repository class will handle all log file transactions.
  *
  * @param profile   the injectible data base profile that will be used.<br>
  *                  MySqlProfile,<br>
  *                  H2Profile, <br>
  *                  etc...<br>
  * @param db        the injectible database object that will be used.<br>
  * @see io.cmdaa.data.dao.LogLineRepoTest
  */
//class LogFileRepo(val profile: JdbcProfile, val dbType: String) {
class LogFileRepo(val profile: JdbcProfile, val db: Database) {
  //  val db = DatabaseConfig.forAnnotation[JdbcProfile].db
  import profile.api._

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action.transactionally), Duration.Inf)


  /** This method will return a list of all log files that are contained
    * within the CMDAA system.
    *
    * @return a list of all the log files in the system
    */
  def findAll(): List[LogFile] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logFiles.result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return the logfile that the user specifies.
    *
    * @param id Log file identifier
    * @return the specified log file.
    */
  def findById(id: Long): Option[LogSource] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logSources.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This method will return the logfile that the user specifies.
    *
    * @param name (the full path name for the log file)
    * @return the specified log file id.
    */
  def findByFileName(name: String): Option[LogSource] = {
    val session = db.createSession()

    try {
      val query = LogFileSchema.logSources.filter(_.name === name).result
      val queryFuture: Future[Seq[LogSource]] = db.run(query)
      val messagesResults = Await.result(queryFuture, Duration.Inf)
      messagesResults.headOption
    } finally {
      session.close()
    }
  }


  /** This method will return the logfile that the user specifies.
    *
    * @param name (the full path name for the log file with a sql wildcard in the name)
    * @return the specified log file.
    */
  def findByFileNameLike(name: String): Option[String] = {
    val session = db.createSession()

    try {

      //    def actionSeq(name: String): DBIO[Seq[String]] = sql"""select ID from LOG_SOURCE where NAME LIKE $name""".as[String]
      def actionSeq(name: String): DBIO[Seq[(String)]] = sql"""select ID from LOG_SOURCE where  cast(meta as json) ->> 'logfilename' LIKE $name""".as[String]


      val messagesResults = Await.result(db.run(actionSeq(name)), Duration.Inf)
      //We are only expecting to see one result returned.
      //If we get zero or more than one we should return None....
      //Better to get None rather than a wrong value that might look right!
      if (messagesResults.size == 1) Some(messagesResults.head) else None
      //if (messagesResults.size < 10 ) Some(messagesResults.head) else None
      //messagesResults.headOption
    } finally {
      session.close()
    }
  }

  /** This method will return the logfile that the user specifies.
    *
    * @param name (the full path name for the log file)
    * @return the specified log file.
    *         *
    *         def findLogSourceByFileName(name: String): Option[LogSource] = {
    *         val query = LogFileSchema.logSources.filter(_.name === name).result
    *         val queryFuture: Future[Seq[LogSource]] = db.run(query)
    *         val messagesResults = Await.result(queryFuture, Duration.Inf)
    *messagesResults.headOption
    *         }
    *         *
    *
    *         This method will return the logfile that the user specifies.
    * @param name (the full path name for the log file with a sql wildcard in the name)
    * @return the specified log file.
    *         *
    *         def findLogSourceByFileNameLike(name: String): Option[String] = {
    *         *
    *         def actionSeq(name: String): DBIO[Seq[String]] = sql"""select ID from LOG_SOURCE where NAME LIKE $name""".as[String]
    *         *
    *         val messagesResults = Await.result(db.run(actionSeq(name)), Duration.Inf)
    *         //We are only expecting to see one result returned.
    *         //If we get zero or more than one we should return None....
    *         //Better to get None rather than a wrong value that might look right!
    *         if (messagesResults.size == 1) Some(messagesResults.head) else None
    *         }
    */


  /** This method will delete the log file that is specified by the user
    *
    * @param id Log file identifier
    * @return the number of records deleted.  It should always equal 0 or 1.
    */
  def deleteById(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logFiles.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will return a list of log files that are associated with a
    * project within the system.  A log file has an exclusive relationship
    * with a project.
    *
    * @param projectId Project identifier
    * @return a list of log files associated with a project.
    */
  def findFilesOwnedByProject(projectId: Long): List[LogFile] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logFiles.filter(_.owningProjectId === projectId).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will insert a logfile.  The logfile must be associated with a project in the system.
    *
    * @param logFile Log file
    */
  def insert(logFile: LogFile): Unit = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logFiles += logFile )
    } finally {
      session.close()
    }
  }

  /**
    * Return a list of all log sources
    *
    * @return List of all log sources
    */
  def listLogSources(): List[LogSource] = {
    val session = db.createSession()

    try {
      exec( logSources.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Return the log source with the specified identifier
    *
    * @param id Log source identifier
    * @return Log source
    */
  def findLogSource(id: Long): Option[LogSource] = {
    val session = db.createSession()

    try {
      exec( logSources.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Insert a log source
    *
    * @param logSource Log source
    * @return The log source with autogenerated identifier
    */
  def insertLogSource(logSource: LogSource): LogSource = {
    val session = db.createSession()

    try {
      exec( logSources returning logSources.map(_.id) into ((logSource, id) => logSource.copy(id = Some(id))) += logSource )
    } finally {
      session.close()
    }
  }

  /**
    * Update a log source
    *
    * @param logSource Log source
    * @return The number of log sources updated
    */
  def updateLogSource(logSource: LogSource): Int = {
    val session = db.createSession()

    try {
      val action = logSources.filter(_.id === logSource.id)
        .map(s => (s.name, s.typeId, s.projectId, s.notifyRowQty, s.meta, s.modifiedBy, s.modifiedDateMillis))
        .update((logSource.name, logSource.typeId, logSource.projectId, logSource.notifyRowQty, logSource.meta, logSource.modifiedBy, logSource.modifiedDateMillis))
      exec( action )
    } finally {
      session.close()
    }
  }

  /**
    * Delete the log source with the specified identifier
    *
    * @param id Log source identifier
    * @return Number of log sources deleted
    */
  def deleteLogSource(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( logSources.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /**
    * Return a list of all log sources belonging to the specified project
    *
    * @param projectId Project identifier
    * @return List of log sources
    */
  def listProjectLogSources(projectId: Long): List[LogSource] = {
    val session = db.createSession()

    try {
      exec( logSources.filter(_.projectId === projectId).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Return a list of all log source types
    *
    * @return List of log source types
    */
  def listLogSourceTypes(): List[LogSourceType] = {
    val session = db.createSession()

    try {
      exec( logSourceTypes.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /**
    * Return the log source type with specified identifier
    *
    * @param id Log source type identifier
    * @return Log source type
    */
  def findLogSourceType(id: Long): Option[LogSourceType] = {
    val session = db.createSession()

    try {
      exec( logSourceTypes.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /**
    * Insert a new log source type
    *
    * @param logSourceType Log source type
    * @return The log source type with autogenerated identifier
    */
  def insertLogSourceType(logSourceType: LogSourceType): LogSourceType = {
    val session = db.createSession()

    try {
      exec( logSourceTypes returning logSourceTypes.map(_.id) into ((logSourceType, id) => logSourceType.copy(id = Some(id))) += logSourceType )
    } finally {
      session.close()
    }
  }

  /**
    * Update and existing log source type
    *
    * @param logSourceType Log source type
    * @return The number of log sources updated
    */
  def updateLogSourceType(logSourceType: LogSourceType): Int = {
    val session = db.createSession()

    try {
      val action = logSourceTypes.filter(_.id === logSourceType.id)
        .map(s => (s.name, s.metaDef, s.modifiedBy, s.modifiedDateMillis))
        .update((logSourceType.name, logSourceType.metaDef, logSourceType.modifiedBy, logSourceType.modifiedDateMillis))
      exec( action )
    } finally {
      session.close()
    }
  }

  /**
    * Delete the log source type for specified identifier
    *
    * @param id Log source type identifier
    * @return The number of log source types deleted
    */
  def deleteLogSourceType(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( logSourceTypes.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

}
