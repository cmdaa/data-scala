package io.cmdaa.data.dao

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.GrokSchema
import io.cmdaa.data.util.entity.GrokSchema.{Grok, GrokLogMessages, _}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.collection.mutable.ListBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

import scala.concurrent.ExecutionContext.Implicits.global

//class GrokRepo(val profile: JdbcProfile, val db: Database) {

class GrokRepo(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  def exec[T](action: DBIO[T]): T =
    Await.result(db.run(action.transactionally), Duration.Inf)

  def insertGrokConfig(config: GrokConfig): GrokConfig = {
    val session = db.createSession()
    try {
      val actions = for {
        grokConfig <- GrokSchema.grokConfigs returning GrokSchema.grokConfigs.map(_.id) into ((grokConfig, id) => grokConfig.copy(id = Some(id))) += config
      } yield grokConfig

      exec( actions )
    } finally {
      session.close()
    }
  }

  def inserGrokAndLogMsgs(grokConfig: GrokConfig, grok: Grok, logMsgs: List[GrokLogMessages]): Unit = {
    insertGrokAndLogMsgs(grokConfig.id.get, grok, logMsgs)
  }

  def insertGrokAndLogMsgs(grokConfig: Long, grok: Grok, logMsgs: List[GrokLogMessages]): Unit = {
    val session = db.createSession()
    try {

      val genGrokLogMsgs = (grok: Grok, logMsgs: List[GrokLogMessages]) => {
        val l = new ListBuffer[GrokLogMessages]()
        logMsgs.foreach(elem => {
          println(grok.id)
          l += GrokLogMessages(None, elem.logMessage, grok.id.get, elem.logSourceId)
        })
        l.toList
      }

      val actions = for {
        grok <- GrokSchema.groks returning GrokSchema.groks.map(_.id) into ((grok, id) => grok.copy(id = Some(id))) += grok
        _ <- DBIO.seq(
          GrokSchema.grokGrokConfigs += new GrokConfigJoin(grokConfig, grok.id.get),
          GrokSchema.grokLogMessages ++= genGrokLogMsgs(grok, logMsgs)
        )
      } yield grok

      exec( actions )
    } finally {
      session.close()
    }
  }

  /** This method will return a list of all grokss that are contained
    * within the CMDAA system.
    *
    * @return a list of all the groks in the system
    */
  def findAllGrokConfigs(): List[GrokConfig] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokConfigs.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return the Grok by logid that the user specifies.
    *
    * @param id grok identifier
    * @return the specified grok.
    */
  def findGrokConfigByLogSourceId(logSourceId: Long): List[GrokConfig] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokConfigs.filter(_.logSourceId === logSourceId).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return the Grok that the user specifies.
    *
    * @param id grok identifier
    * @return the specified grok.
    */
  def findGrokConfigById(id: Long): Option[GrokConfig] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokConfigs.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This project will pull back all users that are associated with the
    * specified project.
    *
    * @param projectId the project to query
    * @return users that are associated with this project
    */
  def findGroksInGrokConfig(configId: Long): List[Grok] = {
    val session = db.createSession()

    try {
      exec( groks.filter(_.id in grokGrokConfigs.filter(_.grokConfigId === configId).map(_.grokId)).sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return a list of all grokss that are contained
    * within the CMDAA system.
    *
    * @return a list of all the groks in the system
    */
  def findAllGroks(): List[Grok] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return the Grok that the user specifies.
    *
    * @param id grok identifier
    * @return the specified grok.
    */
  def findGrokById(id: Long): Option[Grok] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This method will return the Grok that the user specifies.
    *
    * @param uuid grok identifier
    * @return the specified grok.
    */
  def findGrokByUUID(uuid: String): Option[Grok] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.filter(_.uuid === uuid).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This method will insert a grok.  The grok must be associated metadata created for a log source in the system.
    *
    * @param grok
    */
  def insertGrok(grok: Grok): Unit = {
    val session = db.createSession()
    try {
      exec( groks += grok )
    } finally {
      session.close()
    }
  }

  /** This method will batch insert a grok.  The grok must be associated metadata created for a log source in the system.
    *
    * @param grok
    */
  def insertGrok(groks: List[Grok]): Unit = {
    val session = db.createSession()
    try {
      exec( DBIO.sequence( groks.map(row => GrokSchema.groks += row) ) )
    } finally {
      session.close()
    }
  }

  /** This method will delete the grok dthat is specified.
    *
    * @param id
    * @return the number of records deleted.  should be 0 or 1.
    */
  def deleteGrokById(id: Long): Int = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will delete the grok dthat is specified.
    *
    * @param id
    * @return the number of records deleted.  should be 0 or 1.
    */
  def deleteGrokByUUID(uuid: String): Int = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.filter(_.uuid === uuid).delete )
    } finally {
      session.close()
    }
  }

  /** This method will delete the groks that are tied to log source by the metadata ID.
    *
    * @param meta data id
    * @return the number of records deleted.
    */
  def deleteGrokByMetaDataId(metaDataId: Long): Int = {
    val session = db.createSession()
    try {
      exec( GrokSchema.groks.filter(_.logSourceId === metaDataId).delete )
    } finally {
      session.close()
    }
  }

  /** This method will return a list of all log messages associated with a grok that
    * are contained within the CMDAA system.
    *
    * @return a list of all the groks in the system
    */
  def findAllGrokLogMsg(): List[GrokLogMessages] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokLogMessages.sortBy(_.id.asc).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will return the Grok Log Message that the user specifies.
    *
    * @param id grok identifier
    * @return the specified grok.
    */
  def findGrokLogMsgById(id: Long): Option[GrokLogMessages] = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokLogMessages.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This method will insert a grok log message.  The grok must be associated metadata created for a log source in the system.
    *
    * @param grok
    */
  def insertGrokLogMsg(grok: GrokLogMessages): Unit = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokLogMessages += grok )
    } finally {
      session.close()
    }
  }

  /** This method will batch insert a grok log message.  The grok must be associated metadata created for a log source in the system.
    *
    * @param grok
    */
  def insertGrokLogMsg(groks: List[GrokLogMessages]): Unit = {
    val session = db.createSession()
    try {
      exec( DBIO.sequence( groks.map(row => GrokSchema.grokLogMessages += row) ) )
    } finally {
      session.close()
    }
  }

  /** This method will delete the grok dthat is specified.
    *
    * @param id
    * @return the number of records deleted.  should be 0 or 1.
    */
  def deleteGrokLogMsgById(id: Long): Int = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokLogMessages.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will delete the grok dthat is specified.
    *
    * @param id
    * @return the number of records deleted.  should be 0 or 1.
    */
  def deleteGrokLogMsgByLogSourceId(logSourceId: Long): Unit = {
    val session = db.createSession()
    try {
      val drop = DBIO.seq(
        GrokSchema.groks.filter(_.logSourceId === logSourceId).delete andThen
        GrokSchema.grokLogMessages.filter(_.logSourceId === logSourceId).delete
      )

      exec( drop )
    } finally {
      session.close()
    }
  }

  /** This method will delete the groks that are tied to log source by the metadata ID.
    *
    * @param meta data id
    * @return the number of records deleted.
    */
  def deleteGrokLogMsgByGrokId(grokId: Long): Int = {
    val session = db.createSession()
    try {
      exec( GrokSchema.grokLogMessages.filter(_.grokId === grokId).delete )
    } finally {
      session.close()
    }
  }

  def insertGrokAndLogMsgs(grok: Grok, logMsgs: List[GrokLogMessages]): Unit = {
    val session = db.createSession()
    try {


      val genGrokLogMsgs = (grok: Grok, logMsgs: List[GrokLogMessages]) => {
        val l = new ListBuffer[GrokLogMessages]()
        logMsgs.foreach(elem => {
          println(grok.id)
          l += GrokLogMessages(None, elem.logMessage, grok.id.get, elem.logSourceId)
        })
        l.toList
      }

      val actions = for {
        id <- GrokSchema.groks returning GrokSchema.groks.map(_.id) into ((grok, id) => grok.copy(id = Some(id))) += grok
        _ <- DBIO.seq(GrokSchema.grokLogMessages ++= genGrokLogMsgs(id, logMsgs))
      } yield id

      exec( actions )
    } finally {
      session.close()
    }
  }

  /**
    * Return a paginated list of groks for a specified log source
    *
    * @param logSourceId Log Source identifier
    * @param pagination  Optional pagination (offset, limit) tuple
    * @return (Total count of groks for log source, List of groks) tuple
    */
  def listLogSourceGroks(logSourceId: Long, pagination: Option[(Long, Long)] = None): (Int, List[Grok]) = {
    val session = db.createSession()
    try {
      val query = groks.filter(_.logSourceId === logSourceId)
      var page = query.sortBy(_.id.asc)
      if (pagination.nonEmpty) {
        page = page.drop(pagination.get._1).take(pagination.get._2)
      }

      val actions = for {
        count <- query.length.result
        resp <- page.result
      } yield (count, resp)

      val results = exec(actions)
      (results._1, results._2.toList)
    } finally {
      session.close()
    }
  }

  /**
    * Return a paginated list of grok log messages for a specified log source and grok
    *
    * @param logSourceId Log Source identifier
    * @param grokId      Grok identifier
    * @param pagination  Optional pagination (offset, limit) tuple
    * @return (Total count of grok log messages for log source and grok, List of grok log messages) tuple
    */
  def listLogSourceGrokLogsByGrokId(logSourceId: Long, grokId: Long, pagination: Option[(Long, Long)] = None): (Int, List[GrokLogMessages]) = {
    val session = db.createSession()
    try {
      val query = grokLogMessages.filter(msg => msg.logSourceId === logSourceId && msg.grokId === grokId)
      var page = query.sortBy(_.id.asc)
      if (pagination.nonEmpty) {
        page = page.drop(pagination.get._1).take(pagination.get._2)
      }

      val actions = for {
        count <- query.length.result
        resp <- page.result
      } yield (count, resp)

      val results = exec( actions )
      (results._1, results._2.toList)
    } finally {
      session.close()
    }
  }

}