package io.cmdaa.data.dao

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.LogFileSchema
import io.cmdaa.data.util.entity.LogFileSchema.LogLine
import slick.jdbc.JdbcBackend.Database
//import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/** This class will handle the interactions for individual log lines
  * that are tied to a log file.
  *
  * @param profile   the injectible data base profile that will be used.<br>
  *                  MySqlProfile,<br>
  *                  H2Profile, <br>
  *                  etc...<br>
  * @param db        the injectible database object that will be used.<br>
  * @see io.cmdaa.data.dao.LogLineRepoTest
  */
class LogLineRepo(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action.transactionally), Duration.Inf)

  /** This method will return all log lines that are in the database.
    *
    * @note USE THIS CALL WITH CAUTION
    * @return a stream of all log lines.
    */
  def findAll(): Stream[LogLine] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines.result ).toStream
     } finally {
      session.close()
    }
  }

  /** This method will return a log line by the id given.
    *
    * @param id the id of the logline
    * @return the log line
    */
  def findById(id: Long): Option[LogLine] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines.filter(_.id === id).result ).headOption
    } finally {
      session.close()
    }
  }

  /** This method will return all the log lines that are associated
    * with a log file.
    *
    * @param logFileId the log file associated with log lines
    * @return the log lines returned
    */
  def findAllLinesByLogFile(logFileId: Long): Stream[LogLine] = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines.filter(_.logFileId === logFileId).result).toStream
    } finally {
      session.close()
    }
  }

  /** Delete all log lines based on the log file id.  Records that are marked
    * as retain will not be deleted.
    *
    * @param id
    * @return the number of log line records deleted
    */
  def deleteByLogId(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines.filter(_.logFileId === id).filter(!_.retain).delete )
    } finally {
      session.close()
    }
  }

  /** This method will insert a single log line that is associated
    * with a log file.
    *
    * @param logLine the log line to insert.
    */
  def insert(logLine: LogLine): Unit = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines += logLine )
    } finally {
      session.close()
    }
  }

  /** This method will insert a stream of log lines that are associated
    * with a log file.
    *
    * @param lines the lines to insert.
    */
  def insert(lines: Stream[LogLine]): Unit = {
    val session = db.createSession()

    try {
      exec( LogFileSchema.logLines ++= lines )
    } finally {
      session.close()
    }
  }

  /** This method will mark the record so that it cannot be deleted.
    *
    * @param id the record that is to be marked.
    * @return the log line that was marked as retain.
    */
  def markRecordAsRetain(id: Long): LogLine = {
    val session = db.createSession()

    try {
      var logLine = findById(id).get
      val newLogLine = LogLine(Some(6138), logLine.logLine, logLine.logFileId, true)
      exec( DBIO.seq( LogFileSchema.logLines.insertOrUpdate(newLogLine) ) )
      newLogLine
    } finally {
      session.close()
    }
  }

}