package io.cmdaa.data.dao

import com.typesafe.scalalogging.Logger
import io.cmdaa.data.util.entity.LogFileSchema.{LogFile, LogFileTable}
import io.cmdaa.data.util.entity.LogLikelihoodSchema.{LogLikeLihoodRun, LogLikeLihoodRunTable}
import io.cmdaa.data.util.entity.{LogFileSchema, LogLikelihoodSchema}
import slick.jdbc.JdbcBackend.Database
//import slick.jdbc.JdbcBackend.Database
import slick.jdbc.JdbcProfile

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

/** This class will handle the interactions for the log likelihood runs.
  *
  * @param profile   the injectible data base profile that will be used.<br>
  *                  MySqlProfile,<br>
  *                  H2Profile, <br>
  *                  etc...<br>
  * @param db        the injectible database object that will be used.<br>
  * @see io.cmdaa.data.dao.LogLineRepoTest
  */
class LogLikelihoodAnalyticRepo(val profile: JdbcProfile, val db: Database) {

  import profile.api._

  def exec[T](action: DBIO[T]): T = Await.result(db.run(action.transactionally), Duration.Inf)

  /** This method will find all the log likelihood analytic runs that
    * have been done within the system.
    *
    * @return list of log likelihood runs
    */
  def findAll(): List[LogLikeLihoodRun] = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun.result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will find all of the log likeliehood runs that were done
    * on the system and the logfile that the analytic was executed on.
    *
    * @return list of tuples containing log likelihood run and logfiles
    */
  def findAllWithLogFile(): List[(LogLikeLihoodRun, LogFile)] = {
    val session = db.createSession()

    try {
      val query: Query[(LogLikeLihoodRunTable, LogFileTable), (LogLikeLihoodRun, LogFile), Seq] =
        LogLikelihoodSchema.logLikelihoodRun.sortBy(_.runDate) join LogFileSchema.logFiles on ((llRun: LogLikeLihoodRunTable, file: LogFileTable) =>
          llRun.logFileId === file.id
          )
      exec(query.result).toList
     } finally {
      session.close()
    }
  }

  /** This method will find all runs of the log likelihood analytic that
    * were run on a particular log file.
    *
    * @param logFileId the log file that these analytic results are tied to
    * @return a list of log likelihood runs
    */
  def findAllRunsByLogFile(logFileId: Long): List[LogLikeLihoodRun] = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun.filter(_.logFileId === logFileId).result ).toList
    } finally {
      session.close()
    }
  }

  /** This method will insert the results of a loglikelihood run that
    * was run against a log file.
    *
    * @param llRun the log likelihood run to insert
    */
  def insert(llRun: LogLikeLihoodRun): Unit = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun += llRun )
    } finally {
      session.close()
    }
  }

  /** This method will insert a list of loglikelihood runs that
    * were run against any log file.
    *
    * @param llRuns a list of log likelihood runs to insert
    */
  def insert(llRuns: List[LogLikeLihoodRun]): Unit = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun ++= llRuns )
    } finally {
      session.close()
    }
  }

  /** This method will delete the log likelihood run that is specified.
    *
    * @param id
    * @return the number of records deleted.  should be 0 or 1.
    */
  def deleteById(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun.filter(_.id === id).delete )
    } finally {
      session.close()
    }
  }

  /** This method will delete all log likelihood runs that are tied to
    * the specified log.
    *
    * @param id the log id corresponding
    * @return
    */
  def deleteByLogId(id: Long): Int = {
    val session = db.createSession()

    try {
      exec( LogLikelihoodSchema.logLikelihoodRun.filter(_.logFileId === id).delete )
    } finally {
      session.close()
    }
  }
}