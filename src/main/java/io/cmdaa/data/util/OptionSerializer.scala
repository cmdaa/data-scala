package io.cmdaa.data.util

import java.lang.reflect.{ParameterizedType, Type}

import com.google.gson._

class OptionSerializer extends JsonSerializer[Option[_]] with JsonDeserializer[Option[_]] {
  def innerType(outerType: Type) = outerType match {
    case pt: ParameterizedType => pt.getActualTypeArguments()(0)
    case _ => throw new UnsupportedOperationException(outerType.toString)
  }

  def serialize(src: Option[_], typeOfT: Type, context: JsonSerializationContext) =
    src match {
      case Some(v) => println(typeOfT.toString); context.serialize(v, innerType(typeOfT))
      case None => null
    }
  def deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext) =
    json match {
      case null => None
      case _ if json.isJsonNull => None
      case _ => Some(context.deserialize(json, innerType(typeOfT)))
      //      case _ => println(typeOfT.toString); Some(context.deserialize(json, innerType(typeOfT)))
    }
}
