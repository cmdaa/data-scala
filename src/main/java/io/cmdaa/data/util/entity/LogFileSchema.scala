package io.cmdaa.data.util.entity

// import slick.jdbc.MySQLProfile.api._
import slick.jdbc.PostgresProfile.api._

/** This class is the database schema for logfile.  A log file can contain
  * multiple log lines.  Log lines can be marked retained by analytics or
  * users.  The log likelihood analytic will associate log lines with
  * analytic results.  Once a log line has been associated with an analytic
  * result, the record will be marked as retained in the database.
  */
object LogFileSchema {

  /**
    *
    * @param id                 Unique identifier
    * @param name               Log source name
    * @param typeId             Log source type identifier
    * @param notifyRowQty       Notify project owner at row quantity
    * @param meta               Log source meta information
    * @param projectId          Associated project identifier
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class LogSource(id: Option[Long] = None, name: String, typeId: Long, notifyRowQty: Option[Long], meta: String, projectId: Long, modifiedBy: String, modifiedDateMillis: Long)

  final class LogSourceTable(tag: Tag) extends Table[LogSource](tag, "log_source") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def typeId = column[Long]("type_id")
    def notifyRowQty = column[Option[Long]]("notify_row_qty")
    def meta = column[String]("meta", O.SqlType("varchar(500)"))
    def projectId = column[Long]("project_id")
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, name, typeId, notifyRowQty, meta, projectId, modifiedBy, modifiedDateMillis).mapTo[LogSource]

    def project = foreignKey("fk_log_source_001", projectId, UserSchema.projects)(_.id)
    def logSourceType = foreignKey("fk_log_source_002", typeId, LogFileSchema.logSourceTypes)(_.id)
    def idxUniqueLogSource = index("idx_log_source_001", (name, projectId), unique = true)
  }

  lazy val logSources = TableQuery[LogSourceTable]

  /**
    * Log Source Type
    *
    * @param id      Unique identifier
    * @param name    Log source type name
    * @param metaDef Log source meta definition
    */
  case class LogSourceType(id: Option[Long] = None, name: String, metaDef: String, modifiedBy: String, modifiedDateMillis: Long)

  final class LogSourceTypeTable(tag: Tag) extends Table[LogSourceType](tag, "log_source_type") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.SqlType("varchar(100)"))
    def metaDef = column[String]("meta_def", O.SqlType("varchar(500)"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, name, metaDef, modifiedBy, modifiedDateMillis).mapTo[LogSourceType]

    def idxUniqueLogSourceType = index("idx_log_source_type_001", name, unique = true)
  }

  lazy val logSourceTypes = TableQuery[LogSourceTypeTable]

  /** Log File is a log file source that is defined by a user.  The log file or
    * source can be associated with 1 project and 1 project only.
    *
    * @param id              Unique identifier
    * @param name            alias of log source
    * @param logLocation     the location of the log file on the system
    * @param owningProjectId the project that owns this log file
    */
  case class LogFile(id: Long, name: String, logLocation: String, owningProjectId: Long)

  final class LogFileTable(tag: Tag) extends Table[LogFile](tag, "log_file") {
    def id = column[Long]("id", O.PrimaryKey) // This is the primary key column
    def name = column[String]("log_name")
    def logLocation = column[String]("log_location")
    def owningProjectId = column[Long]("owning_project_id")

    def * = (id, name, logLocation, owningProjectId).mapTo[LogFile]

    def owningProject = foreignKey("fk_log_file_001", owningProjectId, UserSchema.projects)(_.id)
  }

  lazy val logFiles = TableQuery[LogFileTable]

  case class LogFileName(fileName: String)

  /** A single log line within a log file.
    *
    * @param id        Unique identifier
    * @param logLine   the log line within the log file
    * @param logFileId the log file containing this log line
    * @param retain    tag for retention
    */
  case class LogLine(id: Option[Long] = None, logLine: String, logFileId: Long, retain: Boolean)

  final class LogLineTable(tag: Tag) extends Table[LogLine](tag, "log_line") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def logLine = column[String]("log_line")
    def logFileId = column[Long]("log_file_id")

    // There will be a many to many relationship with the loglikelihoodrun jobs
    def retain = column[Boolean]("retain")

    def * = (id ?, logLine, logFileId, retain).mapTo[LogLine]

    def logFile = foreignKey("fk_log_line_001", logFileId, logFiles)(_.id)
  }

  lazy val logLines = TableQuery[LogLineTable]

}
