package io.cmdaa.data.util.entity

// import slick.jdbc.MySQLProfile.api._
import slick.jdbc.PostgresProfile.api._

/** This object contains the configuration and results of the log likelihood analytic.<br>
  * @see <a href="https://en.wikipedia.org/wiki/Likelihood_function">Log Likelihoodn</a>
  */
object LogLikelihoodSchema {

  /**
    * The log likelihood threshold:<br>
    * &nbsp;&nbsp;&nbsp;&nbsp;This will allow you to chose how "significant" the terms are for your log templates.<br>
    * The cosine similarity threshold:<br>
    * &nbsp;&nbsp;&nbsp;&nbsp;This threshold is used in the clustering algorithm.  This determines how similar messages
    * &nbsp;&nbsp;&nbsp;&nbsp;must be for them to be clustered together.
    *
    * @param id log likelihood run id
    * @param cosThreshold 0 - 1.  0 being least similar.  1 being most similar.
    * @param logLikelihoodThreshold significance of messages being added to templates
    * @param runDate date of analytic run
    * @param logFileId the logfile corresponding to this configuration
    */
  case class LogLikeLihoodRun(id: Long, cosThreshold:Double, logLikelihoodThreshold:Double, runDate:Long, logFileId:Long)
  final class LogLikeLihoodRunTable(tag: Tag) extends Table[LogLikeLihoodRun](tag, "log_likelihood_run") {
    def id = column[Long]("id", O.PrimaryKey) // This is the primary key column
    def cosThreshold = column[Double]("cos_threshold")
    def logLikelihoodThreshold = column[Double]("log_likelihood_threshold")
    def runDate = column[Long]("run_date")
    def logFileId = column[Long]("log_file_id")

    def * = (id, cosThreshold, logLikelihoodThreshold, runDate, logFileId).mapTo[LogLikeLihoodRun]

    def logFile = foreignKey("fk_log_likelihood_run_001", logFileId, LogFileSchema.logFiles)(_.id)
  }
  lazy val logLikelihoodRun = TableQuery[LogLikeLihoodRunTable]

  /** The results of the analytic run.  Each log line will have it's own results.  Each result will
    * correspond to a cluster.<br>
    * <br>
    * Each result will have a key string that is generated:<br>
    * &nbsp;&nbsp;Original log line:<br>
    * &nbsp;&nbsp;&nbsp;&nbsp;2017-02-15 17:01:04,842 [main] DEBUG io.cmdaa.data: This is an example log message 32m and 14s<br>
    * &nbsp;&nbsp;Key string generated:<br>
    * &nbsp;&nbsp;&nbsp;&nbsp;DATE main DEBUG io.cmdaa.data: This is an example log message NUMm and NUMs<br>
    * &nbsp;&nbsp;Key string generated example with stricter log likelihood policy:<br>
    * &nbsp;&nbsp;&nbsp;&nbsp;DATE main DEBUG io.cmdaa.data: This is an log NUMm and NUMs<br>
    * <br>
    * The relationship of the keystring is a one to many with the log line, the same with the hashed shortened message id.<br>
    * @note the keystring and shortened message id should be put into their own table.
    * @param id
    * @param clusterId the cluster that this log message was assigned to
    * @param keyString the keystring generated for this log message
    * @param originalMessageId the original log message that this result is tied to
    * @param shortenedMessageId the message id of the keystring
    */
  case class LogLikelihoodLine(id: String, clusterId: String, keyString: String, originalMessageId: Long, shortenedMessageId: Long)
  final class LogLikelihoodLineTable(tag: Tag) extends Table[LogLikelihoodLine](tag, "log_line") {
    def id = column[String]("id", O.PrimaryKey) // This is the primary key column
    def clusterId = column[String]("cluster_id")
    def keyString = column[String]("key_string")
    def logLineId = column[Long]("log_line_id")
    def shortenedMessageId = column[Long]("shortened_msg_id")
    def * = (id, clusterId, keyString, logLineId, shortenedMessageId).mapTo[LogLikelihoodLine]

    def logLine = foreignKey("fk_log_line_001", logLineId, LogFileSchema.logLines)(_.id)
  }
}