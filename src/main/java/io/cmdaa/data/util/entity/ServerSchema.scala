package io.cmdaa.data.util.entity

// import slick.jdbc.MySQLProfile.api._
import slick.jdbc.PostgresProfile.api._
/**
  * This schema houses information about the servers defined in the cluster.
  */
object ServerSchema {

  /**
    * This table houses basic information about the server.
    *
    * @param id                 Unique identifier
    * @param fqdn               Fully qualified domain name
    * @param ipAddrV4           IP address v4
    * @param ipAddrV6           IP address v6
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class Server(id: Option[Long] = None, fqdn: String, ipAddrV4: String, ipAddrV6: String, modifiedBy: String, modifiedDateMillis: Long)

  final class ServerTable(tag: Tag) extends Table[Server](tag, "server") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def fqdn = column[String]("fqdn", O.SqlType("varchar(100)"))
    def ipAddrV4 = column[String]("ip_addr_v4", O.SqlType("varchar(50)"))
    def ipAddrV6 = column[String]("ip_addr_v6", O.SqlType("varchar(50)"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, fqdn, ipAddrV4, ipAddrV6, modifiedBy, modifiedDateMillis).mapTo[Server]

    def idxUniqueFqdn = index("idx_server_001", fqdn, unique = true)
  }

  lazy val servers = TableQuery[ServerTable]

  /**
    * This is a label designated to a server, usually by the genders file on a linux system.
    *
    * @param id                 Unique identifier
    * @param name               Label name
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class Label(id: Option[Long] = None, name: String, modifiedBy: String, modifiedDateMillis: Long)

  final class LabelTable(tag: Tag) extends Table[Label](tag, "label") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.SqlType("varchar(100)"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, name, modifiedBy, modifiedDateMillis).mapTo[Label]

    def idxUniqueName = index("idx_label_001", name, unique = true)
  }

  lazy val labels = TableQuery[LabelTable]

  /**
    * A many to one relationship between server labels and the server definition.
    *
    * @param serverId Unique server identifier
    * @param labelId  Unique label identifier
    */
  case class ServerLabel(serverId: Long, labelId: Long)

  final class ServerLabelTable(tag: Tag) extends Table[ServerLabel](tag, "server_label") {
    def serverId = column[Long]("server_id")
    def labelId = column[Long]("label_id")

    def * = (serverId, labelId).mapTo[ServerLabel]

    def pk = primaryKey("pk_server_label", (serverId, labelId))
    def server = foreignKey("fk_server_label_001", serverId, servers)(_.id, onDelete = ForeignKeyAction.Cascade)
    def label = foreignKey("fk_server_label_002", labelId, labels)(_.id)
  }

  lazy val serverLabels = TableQuery[ServerLabelTable]
}
