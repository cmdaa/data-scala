package io.cmdaa.data.util.entity

import slick.jdbc.PostgresProfile.api._

object GrokSchema {

  case class GrokConfig(id: Option[Long] = None, active: Boolean, version: Short, logSourceId: Long, modifiedBy: String, modifiedDateMillis: Long)
  final class GrokConfigTable(tag: Tag) extends Table[GrokConfig](tag, "grok_config") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def active = column[Boolean]("active", O.SqlType("BOOLEAN"))
    def version = column[Short]("version", O.SqlType("SMALLINT"))
    def logSourceId = column[Long]("log_src_id", O.SqlType("BIGINT"))
    def modifiedBy = column[String]("modified_by", O.SqlType("VARCHAR(100)"))
    def modifiedDate = column[Long]("modified_date_millis", O.SqlType("BIGINT"))

    def * = (id ?, active, version, logSourceId, modifiedBy, modifiedDate).mapTo[GrokConfig]
  }

  lazy val grokConfigs = TableQuery[GrokConfigTable]

  case class GrokConfigJoin(grokConfigId: Long, grokId: Long)
  final class GrokConfigJoinTable(tag: Tag) extends Table[GrokConfigJoin](tag, "grok_config_grok") {
    def grokConfigId = column[Long]("grok_config_id")
    def grokId = column[Long]("grok_id")

    def * = (grokConfigId, grokId).mapTo[GrokConfigJoin]

    def pk = primaryKey("pk_a", (grokConfigId, grokId))

    def grokConfg = foreignKey("grok_config_id", grokConfigId, grokConfigs)(_.id, onDelete = ForeignKeyAction.Cascade)
    def grok  = foreignKey("grok__id", grokId, groks)(_.id, onDelete = ForeignKeyAction.Cascade)
  }

  lazy val grokGrokConfigs = TableQuery[GrokConfigJoinTable]

  case class Grok(id: Option[Long] = None, parentId: Long, uuid: String, active: Boolean, grok: String, verified: Boolean, logSourceId: Long)
  final class GrokTable(tag: Tag) extends Table[Grok](tag, "grok") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def parentId = column[Long]("parent_id")
    def uuid = column[String]("uuid", O.SqlType("varchar(36)"))
    def active = column[Boolean]("active", O.SqlType("boolean"))
    def grok = column[String]("grok", O.SqlType("varchar(32000)"))
    def verified = column[Boolean]("verified", O.SqlType("boolean"))
    def logSourceId = column[Long]("meta_data_id")

    def * = (id ?, parentId, uuid, active, grok, verified, logSourceId).mapTo[Grok]
  }

  lazy val groks = TableQuery[GrokTable]

  case class GrokLogMessages(id: Option[Long] = None, logMessage: String, grokId: Long, logSourceId: Long)
  final class GrokLogMsgTable(tag: Tag) extends Table[GrokLogMessages](tag, "grok_log_msg") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def logMessage = column[String]("grok", O.SqlType("varchar(32000)"))
    def grokId = column[Long]("grok_id")
    def logSourceId = column[Long]("log_src_id")

    def * = (id ?, logMessage, grokId, logSourceId).mapTo[GrokLogMessages]
    def grok  = foreignKey("fk_grok_log_msg_001", grokId, GrokSchema.groks)(_.id, onDelete = ForeignKeyAction.Cascade)
    def logSource = foreignKey("fk_grok_log_msg_002", logSourceId, LogFileSchema.logSources)(_.id)
  }

  lazy val grokLogMessages = TableQuery[GrokLogMsgTable]

}