package io.cmdaa.data.util.entity

import io.cmdaa.data.util.entity.LogFileSchema.logFiles
// import slick.jdbc.MySQLProfile.api._
import slick.jdbc.PostgresProfile.api._

/** This class is the database schema for logLineCount.  A log file can contain
  * multiple log lines.  When we trying analyze the whether a line for a particular log is
  * appearing more frequently than "normal" we need to collect counts of these lines and save them
  * to the database.  This table holds the counts for all time of a particular line.
  * Each line is uniquely identified by an MD5 hash that is stored in the table
  
  */
object LogLineCountSchema {

  /** Log line count is a count of the number of times that this line has
    * appeared in a specific log file.  The complete unique index is the log file id and
    * the MD5 hash which uniquely identifies the log line
    * @param id
    * @param log_id
    * @param md5_id
    * @param count
    */
    case class LineCountUpdate(logId: Long, md5Id: String, count: Long)
  //  case class LogLineCount(id: Option[Long] = None, logId: Long, logLineId: Long, md5Id: String, count: Long)
    case class LogLineCount(id: Option[Long] = None, logId: Long, md5Id: String, count: Long)
    final class LogLineCountTable(tag: Tag) extends Table[LogLineCount](tag, "log_line_count") {
      def id = column[Long]("id", O.PrimaryKey, O.AutoInc) // This is the primary key column

      def logId = column[Long]("log_id")
      def md5Id = column[String]("md5_id", O.SqlType("varchar(32)"))
      def count = column[Long]("count")
      // Every table needs a * projection with the same type as the table's type parameter

      def * = (id?, logId, md5Id, count).mapTo[LogLineCount]

    //  def logLine = foreignKey("LOG_LINE_COUNT_LOG_LINE_ID", logLineId, logLines)(_.id)
      
      def log = foreignKey("fk_log_line_count_001", logId, logFiles)(_.id)
      def idx = index("idx_log_line_count_001", (logId, md5Id), unique = true)
    }
    lazy val logLineCounts = TableQuery[LogLineCountTable]

}
