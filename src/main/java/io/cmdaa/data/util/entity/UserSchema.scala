package io.cmdaa.data.util.entity

// import slick.jdbc.MySQLProfile.api._
import slick.jdbc.PostgresProfile.api._

/** This table houses information that pertains to users.
  */
object UserSchema {

  /** A user that has been configured in the system.
    *
    * @param id                 Unique identifier
    * @param username           Username
    * @param passwordHash       Salted password hash
    * @param firstName          First name
    * @param lastName           Last name
    * @param email              Email address
    * @param active             Is user account active
    * @param approved           Has user account been approved
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class User(id: Option[Long] = None, username: String, passwordHash: String, firstName: String, lastName: String, email: String, active: Boolean, approved: Boolean, modifiedBy: String, modifiedDateMillis: Long)

  final class UserTable(tag: Tag) extends Table[User](tag, "user") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def username = column[String]("username", O.SqlType("varchar(100)"))
    def passwordHash = column[String]("password_hash", O.SqlType("varchar(100)"))
    def firstName = column[String]("first_name", O.SqlType("varchar(100)"))
    def lastName = column[String]("last_name", O.SqlType("varchar(100)"))
    def email = column[String]("email", O.SqlType("varchar(250)"))
    def active = column[Boolean]("active")
    def approved = column[Boolean]("approved")
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    // Every table needs a * projection with the same type as the table's type parameter
    def * = (id ?, username, passwordHash, firstName, lastName, email, active, approved, modifiedBy, modifiedDateMillis).mapTo[User]

    def idxUniqueUsername = index("idx_u_user_username", username, unique = true)
  }

  lazy val users = TableQuery[UserTable]

  /** The system is the top level structure in the application.  The system houses project
    * users, groups, etc...
    *
    * @param id                 Unique identifier
    * @param name               System name
    * @param description        System description
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class System(id: Option[Long] = None, name: String, description: String, modifiedBy: String, modifiedDateMillis: Long)

  final class SystemTable(tag: Tag) extends Table[System](tag, "system") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.SqlType("varchar(100)"))
    def description = column[String]("description", O.SqlType("varchar(250)"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, name, description, modifiedBy, modifiedDateMillis).mapTo[System]

    def idxUniqueName = index("idx_system_001", name, unique = true)
  }

  lazy val systems = TableQuery[SystemTable]

  /**
    *
    * @param id                 Unique identifier
    * @param name               Project name
    * @param systemId           Unique parent system identifier
    * @param ownerId            User identifier of project owner
    * @param suggestRegEx       Suggest initial regular expression
    * @param regExQuantity      Suggest regular expression at quantity of units
    * @param regExUnit          Suggest regular expression at quantity of units
    * @param deployStatus       Deployment status (PENDING, TESTING, PRODUCTION or REJECTED)
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class Project(
                      id: Option[Long] = None,
                      name: String,
                      systemId: Long,
                      ownerId: Long,
                      suggestRegEx: Boolean,
                      regExQuantity: Option[Long],
                      regExUnit: Option[String],
                      deployStatus: String,
                      modifiedBy: String,
                      modifiedDateMillis: Long)

  final class ProjectTable(tag: Tag) extends Table[Project](tag, "project") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc) // This is the primary key column
    def name = column[String]("name", O.SqlType("varchar(100)"))
    def systemId = column[Long]("system_id")
    def ownerId = column[Long]("owner_id")
    def suggestRegEx = column[Boolean]("suggest_rex_ex")
    def regExQuantity = column[Option[Long]]("reg_ex_quantity")
    def regExUnit = column[Option[String]]("reg_ex_unit", O.SqlType("varchar(100)"))
    def deployStatus = column[String]("deploy_status", O.SqlType("varchar(25)"), O.Default("PENDING"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    // Every table needs a * projection with the same type as the table's type parameter
    def * = (id ?, name, systemId, ownerId, suggestRegEx, regExQuantity, regExUnit, deployStatus, modifiedBy, modifiedDateMillis).mapTo[Project]

    def system = foreignKey("fk_project_001", systemId, systems)(_.id)
    def owner = foreignKey("fk_project_002", ownerId, users)(_.id)
  }

  lazy val projects = TableQuery[ProjectTable]

  /**
    * Projects deployed in either TEST or PRODUCTION
    *
    * @param projectId          Project identifier
    * @param deployedType       Deployment type (TEST or PRODUCTION)
    * @param deployedBy         Deployed by user
    * @param deployedDateMillis Date of deployment
    */
  case class ProjectDeployed(
                              projectId: Long,
                              deployedType: String,
                              deployedBy: String,
                              deployedDateMillis: Long
                            )

  final class ProjectDeployedTable(tag: Tag) extends Table[ProjectDeployed](tag, "project_deployed") {
    def projectId = column[Long]("project_id")
    def deployedType = column[String]("deployed_type", O.SqlType("varchar(25)"), O.Default("TEST"))
    def deployedBy = column[String]("deployed_by", O.SqlType("varchar(100)"))
    def deployedDateMillis = column[Long]("deployed_date_millis")

    def * = (projectId, deployedType, deployedBy, deployedDateMillis).mapTo[ProjectDeployed]

    def pk = primaryKey("pk_project_deployed", (projectId, deployedDateMillis))
    def fkProject = foreignKey("fk_project_deployed_001", projectId, projects)(_.id)
  }

  lazy val projectsDeployed = TableQuery[ProjectDeployedTable]

  /**
    * Projects rejected
    *
    * @param projectId          Project identifier
    * @param rejectedReason     Reason project was rejected
    * @param rejectedBy         Rejected by user
    * @param rejectedDateMillis Date of rejection
    */
  case class ProjectRejected(
                              projectId: Long,
                              rejectedReason: String,
                              rejectedBy: String,
                              rejectedDateMillis: Long
                            )

  final class ProjectRejectedTable(tag: Tag) extends Table[ProjectRejected](tag, "project_rejected") {
    def projectId = column[Long]("project_id")
    def rejectedReason = column[String]("rejected_reason", O.SqlType("varchar(250)"))
    def rejectedBy = column[String]("rejected_by", O.SqlType("varchar(100)"))
    def rejectedDateMillis = column[Long]("rejected_date_millis")

    def * = (projectId, rejectedReason, rejectedBy, rejectedDateMillis).mapTo[ProjectRejected]

    def pk = primaryKey("pk_project_rejected", (projectId, rejectedDateMillis))
    def fkProject = foreignKey("fk_project_rejected_001", projectId, projects)(_.id)
  }

  lazy val projectsRejected = TableQuery[ProjectRejectedTable]

  // eg... superuser, admin, user
  /** Users role in the system/project.  superuser, owner, developers, etc...
    *
    * @param id                 Unique identifier
    * @param name               Role name
    * @param description        Role description
    * @param modifiedBy         Last modified by user
    * @param modifiedDateMillis Date record was last modified
    */
  case class Role(id: Option[Long] = None, name: String, description: String, modifiedBy: String, modifiedDateMillis: Long)

  final class RoleTable(tag: Tag) extends Table[Role](tag, "role") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.SqlType("varchar(100)"))
    def description = column[String]("description", O.SqlType("varchar(250)"))
    def modifiedBy = column[String]("modified_by", O.SqlType("varchar(100)"))
    def modifiedDateMillis = column[Long]("modified_date_millis")

    def * = (id ?, name, description, modifiedBy, modifiedDateMillis).mapTo[Role]

    def idxUniqueName = index("idx_role_001", name, unique = true)
  }

  lazy val roles = TableQuery[RoleTable]

  /**
    * Houses the users of a project and their assigned roles within the project.
    *
    * @param userId    User identifier
    * @param projectId Project identifier
    * @param roleId    Role identifier
    */
  case class UserProject(userId: Long, projectId: Long, roleId: Long)

  final class UserProjectTable(tag: Tag) extends Table[UserProject](tag, "project_user") {
    def userId = column[Long]("user_id")
    def projectId = column[Long]("project_id")
    def roleId = column[Long]("role_id")

    def * = (userId, projectId, roleId).mapTo[UserProject]

    def pk = primaryKey("pk_project_user", (userId, projectId, roleId))
    def user = foreignKey("fk_project_user_001", userId, users)(_.id, onDelete = ForeignKeyAction.Cascade)
    def project = foreignKey("fk_project_user_002", projectId, projects)(_.id, onDelete = ForeignKeyAction.Cascade)
    def role = foreignKey("fk_project_user_003", roleId, roles)(_.id)
  }

  lazy val userProjects = TableQuery[UserProjectTable]

  /**
    * Notification
    *
    * @param id               Unique identifier
    * @param message          Message
    * @param severity         Severity
    * @param issuedDateMillis Date notification was issued
    */
  case class Notification(id: Option[Long] = None, message: String, severity: Int, issuedDateMillis: Long)

  final class NotificationTable(tag: Tag) extends Table[Notification](tag, "notification") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def message = column[String]("message", O.SqlType("varchar(500)"))
    def severity = column[Int]("severity")
    def issuedDateMillis = column[Long]("issued_date_millis")

    def * = (id ?, message, severity, issuedDateMillis).mapTo[Notification]
  }

  lazy val notifications = TableQuery[NotificationTable]

  /**
    * Notifications for user and/or project
    *
    * @param userId         Unique user identifier (or None)
    * @param projectId      Unique project identifier (or None)
    * @param notificationId Unique notification identifier
    */
  case class UserProjectNotification(userId: Option[Long], projectId: Option[Long], notificationId: Long, ackDateMillis: Option[Long] = None)

  final class UserProjectNotificationTable(tag: Tag) extends Table[UserProjectNotification](tag, "user_project_notification") {
    def userId = column[Option[Long]]("user_id")
    def projectId = column[Option[Long]]("project_id")
    def notificationId = column[Long]("notification_id")
    def ackDateMillis = column[Option[Long]]("ack_date_millis")

    def * = (userId, projectId, notificationId, ackDateMillis).mapTo[UserProjectNotification]

    def fkUser = foreignKey("fk_user_project_notification_001", userId, users)(_.id.?, onDelete = ForeignKeyAction.Cascade)
    def fkProject = foreignKey("fk_user_project_notification_002", projectId, projects)(_.id.?, onDelete = ForeignKeyAction.Cascade)
    def fkNotification = foreignKey("fk_user_project_notification_003", notificationId, notifications)(_.id, onDelete = ForeignKeyAction.Cascade)
  }

  lazy val userProjectNotifications = TableQuery[UserProjectNotificationTable]

  /**
    * Access token for OAuth
    *
    * @param uuid                 Unique universal identifier
    * @param userId               User identifier
    * @param lastAccessDateMillis date last accessed in milliseconds since epoch
    */
  case class AccessToken(uuid: Option[String], userId: Long, lastAccessDateMillis: Long)

  final class AccessTokenTable(tag: Tag) extends Table[AccessToken](tag, "access_token") {
    def uuid = column[String]("id", O.PrimaryKey, O.SqlType("varchar(100)"))
    def userId = column[Long]("user_id")
    def lastAccessDateMillis = column[Long]("last_access_date_millis")

    def * = (uuid ?, userId, lastAccessDateMillis).mapTo[AccessToken]

    def user = foreignKey("fk_access_token_001", userId, users)(_.id)
  }

  lazy val accessTokens = TableQuery[AccessTokenTable]

  /**
    * User specific application properties
    *
    * @param userId User identifier
    * @param key    Property key
    * @param value  Property value
    */
  case class UserApplicationProperty(userId: Long, key: String, value: String)

  final class UserApplicationPropertyTable(tag: Tag) extends Table[UserApplicationProperty](tag, "user_application_properties") {
    def userId = column[Long]("user_id")
    def key = column[String]("key", O.SqlType("varchar(100)"))
    def value = column[String]("value", O.SqlType("varchar(500)"))

    def * = (userId, key, value).mapTo[UserApplicationProperty]

    def pk = primaryKey("pk_user_app_prop", (userId, key))
    def fkUser = foreignKey("fk_user_app_prop_001", userId, users)(_.id)
  }

  lazy val userApplicationProperties = TableQuery[UserApplicationPropertyTable]

}