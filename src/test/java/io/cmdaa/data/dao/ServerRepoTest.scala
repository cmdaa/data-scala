package io.cmdaa.data.dao

import java.sql.SQLException

import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.ServerSchema._
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.dbio.DBIO
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class ServerRepoTest extends FlatSpec with BeforeAndAfter {

   val serverRepo = new ServerRepo(H2Profile, Database.forConfig("h2"))

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()
    val setup = DBIO.seq(
      (servers.schema ++ labels.schema ++ serverLabels.schema).create,
      servers += Server(Some(1L), "foo.com", "127.0.0.1", "::1/128", "modifier", 10000),
      servers += Server(Some(2L), "bar.com", "127.0.0.1", "::1/128", "modifier", 10000),
      servers += Server(Some(3L), "baz.com", "127.0.0.1", "::1/128", "modifier", 10000),
      servers += Server(Some(4L), "bazinga.com", "127.0.0.1", "::1/128", "modifier", 10000),
      servers += Server(Some(5L), "test", "127.0.0.1", "::1/128", "modifier", 10000),
      labels += Label(Some(1L), "hdfs", "modifier", 10000),
      labels += Label(Some(2L), "zookeeper", "modifier", 10000),
      labels += Label(Some(3L), "httpd", "modifier", 10000),
      labels += Label(Some(4L), "nginx", "modifier", 10000),
      labels += Label(Some(5L), "tomcat", "modifier", 10000),
      labels += Label(Some(6L), "test", "modifier", 10000),
      serverLabels += ServerLabel(1L, 1L),
      serverLabels += ServerLabel(1L, 2L),
      serverLabels += ServerLabel(1L, 3L),
      serverLabels += ServerLabel(1L, 4L),
      serverLabels += ServerLabel(2L, 1L),
      serverLabels += ServerLabel(2L, 2L),
      serverLabels += ServerLabel(3L, 3L),
      serverLabels += ServerLabel(3L, 4L)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)
    session.close()
  }

  after {
    val session = db.createSession()
    val query = DBIO.seq(
      serverLabels.schema.drop andThen
        labels.schema.drop andThen
        servers.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)
    session.close()
  }

  "Listing labels" should "return all the labels" in {
    assert(serverRepo.listLabels().size == 6)
  }

  "Finding a label" should "find the correct label specified by id" in {
    assert(serverRepo.findLabel(6).get.name == "test")
  }

  "Inserting a label" should "add a label to the list above" in {
    val label: Label = Label(None, "inserted", "modifier", 10000)
    assert(label.id.isEmpty)
    val labelWithId: Label = serverRepo.insertLabel(label)
    assert(labelWithId.id.nonEmpty)
    assert(serverRepo.listLabels().size == 7)
  }

  "Inserting a null label name" should "produce an SQL exception" in {
    assertThrows[SQLException] {
      serverRepo.insertLabel(Label(None, null, "modifier", 10000))
    }
  }

  "Inserting a duplicate label name" should "produce an SQL exception" in {
    assertThrows[SQLException] {
      serverRepo.insertLabel(Label(None, "hdfs", "modifier", 10000))
    }
  }

  "Updating a label" should "update the correct label" in {
    assert(serverRepo.findLabel(6).get.name == "test")
    val label: Label = Label(Some(6L), "updated", "modifier", 10000)
    assert(serverRepo.updateLabel(label) == 1)
    assert(serverRepo.findLabel(6).get.name == "updated")
  }

  "Deleting a label" should "delete the correct label" in {
    val label: Label = serverRepo.findLabel(6).get
    assert(label.id.get == 6L)
    assert(label.name == "test")
    assert(serverRepo.deleteLabel(6) == 1)
    assert(serverRepo.listLabels().size == 5)
  }

  "listServers" should "return all servers inserted above" in {
    assert(serverRepo.listServers().size == 5)
  }

  "findServer" should "should return a server" in {
    assert(serverRepo.findServer(5).get.fqdn == "test")
  }

  "Inserting a server" should "insert a server successfully" in {
    val server:Server = Server(None, "fizz.com", "127.0.0.1", "::1/128", "modifier", 10000)
    assert(server.id.isEmpty)
    val serverWithId: Server = serverRepo.insertServer(server)
    assert(serverWithId.id.nonEmpty)
    assert(serverRepo.listServers().size == 6)
  }

  "Inserting a server with a list of related labels" should "insert a server successfully" in {
    val l1 = serverRepo.insertLabel(Label(None, "foo", "modifier", 10000))
    val l2 = serverRepo.insertLabel(Label(None, "bar", "modifier", 10000))
    val labels = List(l1, l2)
    val server:Server = Server(None, "foo.bar", "127.0.0.1", "::1/128", "modifier", 10000)
    assert(server.id.isEmpty)
    val serverWithId: Server = serverRepo.insertServer(server, labels)
    assert(serverWithId.id.nonEmpty)
    assert(serverRepo.listServers().size == 6)
    assert(serverRepo.findLabelsByServer(serverWithId.id.get).size == 2)
    assert(serverRepo.findServersByLabel(l1.id.get).size == 1)
    assert(serverRepo.findServersByLabel(l2.id.get).size == 1)
  }

  "Inserting a null server fqdn" should "produce an SQL exception" in {
    assertThrows[SQLException] {
      serverRepo.insertServer(Server(None, null, "127.0.0.1", "::1/128", "modifier", 10000))
    }
  }

  "Inserting a duplicate server fqdn" should "produce an SQL exception" in {
    assertThrows[SQLException] {
      serverRepo.insertServer(Server(None, "foo.com", "127.0.0.1", "::1/128", "modifier", 10000))
    }
  }

  "updateServer" should "update a server" in {
    assert(serverRepo.findServer(5).get.fqdn == "test")
    val server: Server = Server(Some(5L), "updated", "127.0.0.1", "::1/128", "modifier", 10000)
    assert(serverRepo.updateServer(server) == 1)
    assert(serverRepo.findServer(5).get.fqdn == "updated")
  }

  "Updating a server with related list of labels" should "update the server and label relationship" in {
    var server: Server = serverRepo.findServer(1).get
    assert(server.fqdn == "foo.com")
    var labels: List[Label] = serverRepo.findLabelsByServer(server.id.get)
    assert(labels.size == 4)
    val l1 = serverRepo.insertLabel(Label(None, "foo", "modifier", 10000))
    val l2 = serverRepo.insertLabel(Label(None, "bar", "modifier", 10000))
    labels = List(l1, l2)
    server = Server(Some(1L), "updated", "127.0.0.1", "::1/128", "modifier", 10000)
    assert(serverRepo.updateServer(server, labels) == 1)
    assert(serverRepo.findServer(1).get.fqdn == "updated")
    assert(serverRepo.findLabelsByServer(server.id.get).size == 2)
  }

  "deleteServer" should "delete a server" in {
    val server: Server = serverRepo.findServer(5).get
    assert(server.id.get == 5L)
    assert(server.fqdn == "test")
    assert(serverRepo.deleteServer(5) == 1)
    assert(serverRepo.listServers().size == 4)
  }

  "Deleting a server with associated labels" should "work and cascade deletes to FK" in {
    val server: Server = serverRepo.findServer(1).get
    assert(server.id.get == 1L)
    assert(server.fqdn == "foo.com")
    assert(serverRepo.deleteServer(1) == 1)
    assert(serverRepo.listServers().size == 4)
    assert(serverRepo.findLabelsByServer(1).isEmpty)
  }

  "Deleting a label with associated servers" should "throw an exception" in {
    val label: Label = serverRepo.findLabel(1).get
    assert(label.id.get == 1L)
    assert(label.name == "hdfs")
    assertThrows[SQLException] {
      assert(serverRepo.deleteLabel(1) == 1)
    }
  }

  "Inserting a server label relation for a valid server and valid label" should "work" in {
    serverRepo.insertServerLabel(ServerLabel(4L, 1L))
    assert(serverRepo.findLabelsByServer(4L).size == 1)
    assert(serverRepo.findServersByLabel(1L).size == 3)
  }

  "Inserting a list of server label relations for valid servers and valid labels" should "work" in {
    val serverLabelList: List[ServerLabel] = List(
      ServerLabel(4L, 1L), ServerLabel(4L, 2L), ServerLabel(4L, 3L)
    )
    serverRepo.insertServerLabels(serverLabelList)
    assert(serverRepo.findLabelsByServer(4L).size == 3)
    assert(serverRepo.findServersByLabel(1L).size == 3)
    assert(serverRepo.findServersByLabel(2L).size == 3)
    assert(serverRepo.findServersByLabel(3L).size == 3)
  }

  "Deleting a server label relation for a valid server and valid label" should "work" in {
    serverRepo.deleteServerLabel(1L, 1L)
    assert(serverRepo.findLabelsByServer(1L).size == 3)
    assert(serverRepo.findServersByLabel(1L).size == 1)
  }

  "getServersByLabel" should "return all servers labeled a certain way" in {
    assert(serverRepo.findServersByLabel(1).size == 2)
  }

  "getLabelsForServer" should "return all the labels for a certain server" in {
    assert(serverRepo.findLabelsByServer(1).size == 4)
  }
}
