package io.cmdaa.data.dao

import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.LogFileSchema.{LogFile, LogLine, logFiles, logLines}
import io.cmdaa.data.util.entity.UserSchema.{Project, System, User, projects, systems, users}
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._

//import slick.jdbc.MySQLProfile
//import slick.jdbc.MySQLProfile.api._


import java.lang.{System => JavaSystem}

import slick.jdbc.JdbcBackend.Database

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}


@RunWith(classOf[JUnitRunner])
class LogLineRepoTest extends FlatSpec with BeforeAndAfter {

  //  val db = Database.forConfig("mariadb")
  //  val logLineRepo = new LogLineRepo(MySQLProfile, db)


  val logLineRepo = new LogLineRepo(H2Profile, Database.forConfig("h2"))

//  logLineRepo.connect()

  //  val logFilesTable = TableQuery[LogFileTable]
  //  val logLineTable = TableQuery[LogLineTable]

  var logLinesList = new ListBuffer[LogLine]

  {
    val lines = scala.io.Source.fromInputStream(getClass.getResourceAsStream("/test-logs/apache.log")).getLines
    lines.foreach(logLinesList += LogLine(None, _, 1, retain = false))
  }

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()
    val setup = DBIO.seq(
      (
        systems.schema ++ users.schema ++ logFiles.schema ++ logLines.schema ++ projects.schema).create,
      systems += System(Some(1), "system test", "cloud 3", "mgpohl", JavaSystem.currentTimeMillis()),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "project1", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      logFiles += LogFile(1, "messages", "/var/log/apache", 1)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)


    val insert = DBIO.seq(
      logLines ++= logLinesList.toStream
    )
    val insertFuture: Future[Unit] = db.run(insert.transactionally)
    Await.result(insertFuture, Duration.Inf)
    session.close()
  }

  after {
    val session = db.createSession()
    val query = DBIO.seq(
      logLines.schema.drop andThen
        logFiles.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        systems.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)
    session.close()
  }

  "findAll" should "should return 10000 records" in {
    assert(10000 == logLineRepo.findAll().size)
  }

  "findAllLinesByLogFile hit" should "return 10000 records" in {
    assert(10000 == logLineRepo.findAllLinesByLogFile(1).size)
  }

  "findAllLinesByLogFile miss" should "return 0 records" in {
    assert(0 == logLineRepo.findAllLinesByLogFile(2).size)
  }

  "insert" should "should add 1 record" in {
    logLineRepo.insert(LogLine(None, "TEST LOG MESSAGE", 1, retain = false))
    assert(10001 == logLineRepo.findAll().size)
  }

  "insert list" should "should add 10000 records" in {
    logLineRepo.insert(logLinesList.toStream)
    assert(20000 == logLineRepo.findAll().size)
  }

  "deleteByLogId" should "should delete all log lines" in {
    assert(10000 == logLineRepo.deleteByLogId(1))
  }

  "findById" should "return a single logline" in {
    6138L == logLineRepo.findById(6138).get.id.get
  }

  "markRecordAsRetain" should "mark record as a keep" in {
    assert(logLineRepo.markRecordAsRetain(6138L).retain)
  }
}
