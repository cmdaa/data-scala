package io.cmdaa.data.dao

import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.LogFileSchema._
import io.cmdaa.data.util.entity.UserSchema._
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
//import slick.jdbc.MySQLProfile
//import slick.jdbc.MySQLProfile.api._
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class LogFileRepoTest extends FlatSpec with BeforeAndAfter {
  val logFileRepo = new LogFileRepo(H2Profile, Database.forConfig("h2" ))

  //  logFileRepo.connect()
  //  val db = Database.forConfig("mariadb" )
  //  val logFileRepo = new LogFileRepo(MySQLProfile, db)
  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()

    val setup = DBIO.seq(
      (systems.schema ++ users.schema ++ logSourceTypes.schema ++ logSources.schema ++ logFiles.schema ++ projects.schema).create,
      systems += System(Some(1), "cloud 1", "This is the cloud 1 system", "modifier", 10000),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "project one", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      projects += Project(Some(2), "project two", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      logFiles += LogFile(1, "messages", "/var/log/apache", 1),
      logFiles += LogFile(2, "nginx", "/var/log/nginx", 1),
      logFiles += LogFile(3, "httpd", "/var/log/httpd", 1),
      logSourceTypes += LogSourceType(Some(1), "CMDAA", "{}", "modifier", 10000),
      logSources += LogSource(Some(1), "messages", 1, Some(500), "{}", 1, "modifier", 10000),
      logSources += LogSource(Some(2), "nginx", 1, Some(600), "{}", 1, "modifier", 10000),
      logSources += LogSource(Some(3), "httpd", 1, Some(700), "{}", 2, "modifier", 10000)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)

    session.close()
  }

  after {
    val session = db.createSession()

    val query = DBIO.seq(
      logSources.schema.drop andThen
        logSourceTypes.schema.drop andThen
        logFiles.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        systems.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)

    session.close()
  }

  "findAll" should "should return 3 records" in {
    assert(3 == logFileRepo.findAll().size)

  }

  "findById" should "return the same id queried" in {
    // assert(logFileRepo.findById(2).get.id == 2)
    assert(logFileRepo.findById(2).get.id.getOrElse(None) == 2L)
  }

  "findFilesOwnedByProject" should "" in {
    assert(logFileRepo.findFilesOwnedByProject(1).size == 3)
  }

  "deleteById" should "reduce the number of logfiles" in {
    assert(logFileRepo.deleteById(2) == 1)
  }

  "insert" should "increase the total number" in {
    logFileRepo.insert(LogFile(4, "httpd", "/var/log/hdfs", 1))
    assert(4 == logFileRepo.findAll().size)
  }

  "listing all log sources" should "return all three log sources" in {
    assert(3 === logFileRepo.listLogSources().size)
  }

  "listing all log sources for a specified project" should "return the corresponding log sources" in {
    assert(2 === logFileRepo.listProjectLogSources(1).size)
    assert(1 === logFileRepo.listProjectLogSources(2).size)
  }

  "finding a log source by its identifier" should "find the correct log source" in {
    val logSource: LogSource = logFileRepo.findLogSource(1).get
    assert(1L === logSource.id.get)
    assert("messages" === logSource.name)
    assert(500 === logSource.notifyRowQty.get)
    assert(1 === logSource.projectId)
    assert("modifier" === logSource.modifiedBy)
    assert(10000 === logSource.modifiedDateMillis)
  }

  "inserting a log source" should "insert a log source and generate an id" in {
    val logSource: LogSource = logFileRepo.insertLogSource(LogSource(None, "new source", 1, Some(800), "{}", 2, "modifier", 20000))
    assert(logSource.id.nonEmpty)
    assert(4 === logFileRepo.listLogSources().size)
    assert("new source" === logSource.name)
    assert(1 === logSource.typeId)
    assert(800 === logSource.notifyRowQty.get)
    assert("{}" === logSource.meta)
    assert(2 === logSource.projectId)
    assert("modifier" === logSource.modifiedBy)
    assert(20000 === logSource.modifiedDateMillis)
  }

  "updating a log source" should "correctly update the log source" in {
    assert(1 === logFileRepo.updateLogSource(LogSource(Some(1), "updated", 1, Some(400), "{}", 2, "modder", 40000)))
    val logSource: LogSource = logFileRepo.findLogSource(1).get
    assert(1L === logSource.id.get)
    assert("updated" === logSource.name)
    assert(1 === logSource.typeId)
    assert(400 === logSource.notifyRowQty.get)
    assert("{}" === logSource.meta)
    assert(2 === logSource.projectId)
    assert("modder" === logSource.modifiedBy)
    assert(40000 === logSource.modifiedDateMillis)
  }

  "deleting a log source" should "delete the correct log source" in {
    assert(1 === logFileRepo.deleteLogSource(1))
    assert(2 === logFileRepo.listLogSources().size)
    assert(None === logFileRepo.findLogSource(1))
  }

  "crud operations on log source types" should "work correctly" in {
    var s3 = logFileRepo.insertLogSourceType(LogSourceType(None, "S3", "{'foo': 'string'}", "modone", 2000000))
    assert(s3.id.nonEmpty)
    assert("S3" === s3.name)
    assert("{'foo': 'string'}" === s3.metaDef)
    assert("modone" === s3.modifiedBy)
    assert(2000000 === s3.modifiedDateMillis)
    var upload = logFileRepo.insertLogSourceType(LogSourceType(None, "File Upload", "{'bar': 'string'}", "modtwo", 4000000))
    assert(upload.id.nonEmpty)
    assert("File Upload" === upload.name)
    assert("{'bar': 'string'}" === upload.metaDef)
    assert("modtwo" === upload.modifiedBy)
    assert(4000000 === upload.modifiedDateMillis)
    assert(s3.id.get !== upload.id.get)
    assert(3 === logFileRepo.listLogSourceTypes().size)
    s3 = logFileRepo.findLogSourceType(s3.id.get).get
    assert(s3.id.nonEmpty)
    assert("S3" === s3.name)
    assert("{'foo': 'string'}" === s3.metaDef)
    assert("modone" === s3.modifiedBy)
    assert(2000000 === s3.modifiedDateMillis)
    assert(1 === logFileRepo.updateLogSourceType(LogSourceType(upload.id, "Client Upload", "{'bar': 'long'}", "modthree", 3000000)))
    upload = logFileRepo.findLogSourceType(upload.id.get).get
    assert(upload.id.nonEmpty)
    assert("Client Upload" === upload.name)
    assert("{'bar': 'long'}" === upload.metaDef)
    assert("modthree" === upload.modifiedBy)
    assert(3000000 === upload.modifiedDateMillis)
    assert(1 === logFileRepo.deleteLogSourceType(upload.id.get))
    assert(logFileRepo.findLogSourceType(upload.id.get).isEmpty)
    assert(2 === logFileRepo.listLogSourceTypes().size)
  }
}
