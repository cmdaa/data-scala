package io.cmdaa.data.dao

import io.cmdaa.data.util.entity.LogFileSchema.{LogFile, LogLine, logFiles, logLines}
import io.cmdaa.data.util.entity.LogLineCountSchema._
import io.cmdaa.data.util.entity.UserSchema.{Project, System, User, projects, systems, users}
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._

//import slick.jdbc.MySQLProfile
//import slick.jdbc.MySQLProfile.api._


import java.lang.{System => JavaSystem}

import slick.jdbc.JdbcBackend.Database

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}


@RunWith(classOf[JUnitRunner])
class LogLineCountRepoTest extends FlatSpec with BeforeAndAfter {

  //  val db = Database.forConfig("mariadb")
  //   val logLineCountRepo = new LogLineCountRepo(MySQLProfile, db)


  val logLineCountRepo = new LogLineCountRepo(H2Profile, Database.forConfig("h2"))
//  logLineCountRepo.connect()
  var logLineList = new ListBuffer[LogLine]

  {
    val lines = scala.io.Source.fromInputStream(getClass.getResourceAsStream("/test-logs/apache.log")).getLines
    lines.foreach(logLineList += LogLine(None, _, 1, retain = false))
  }

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()

    //The first section of this code just sets up some data that should be in the database
    //before the LogLineCount operations will even work
    val setup = DBIO.seq(
      (systems.schema ++ users.schema ++ logFiles.schema ++ logLines.schema ++ projects.schema ++ logLineCounts.schema).create,

      systems += System(Some(1), "system test", "cloud 3", "mgpohl", JavaSystem.currentTimeMillis()),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "project1", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      logFiles += LogFile(1, "messages", "/var/log/apache", 1)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)

    val insert = DBIO.seq(
      logLines ++= logLineList.toStream
    )

    val insertFuture: Future[Unit] = db.run(insert.transactionally)
    Await.result(insertFuture, Duration.Inf)
    //This ends the preliminary work to set up the environment

    //Here we insert some test data into the LOG_LINE_COUNT table
    //This test is not complete because the process that will create and update these records will be written
    //in ruby and I don't know how to integrate that for a unit test here.  I hope to write a unit test in the
    //ruby code but I'm not sure how to do that yet....for now this is the best we can do.
    /*  val logLineSeq: Stream[LogLineCount] =  Stream.empty :+
       LogLineCount(Some(1),1,1,"fake md5-value-1", 1) :+
       LogLineCount(Some(2),1,2,"fake md5-value-2", 2) :+
       LogLineCount(Some(3),1,3,"fake md5-value-3", 3)

     logLineCountRepo.insert(logLineSeq)
    */
    logLineCountRepo.insert(LogLineCount(Some(1), 1, "fake md5-value-1", 1))
    logLineCountRepo.insert(LogLineCount(Some(2), 1, "fake md5-value-2", 2))
    logLineCountRepo.insert(LogLineCount(Some(3), 1, "fake md5-value-3", 3))
    session.close()
  }

  after {
    val session = db.createSession()
    val query = DBIO.seq(
      logLineCounts.schema.drop andThen
        logLines.schema.drop andThen
        logFiles.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        systems.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)
    session.close()

  }

  "findAllLinesByLogFile miss" should "return 0 records" in {
    assert(0 == logLineCountRepo.findAllLinesByLogFile(2).size)
  }

  "findAllLinesByLogFile hit" should "return 3 records" in {
    assert(3 == logLineCountRepo.findAllLinesByLogFile(1).size)
  }

  "insert" should "should add 1 record" in {
    logLineCountRepo.insert(LogLineCount(None, 1, "fake md5-value-4", 4))
    assert(4 == logLineCountRepo.findAllLinesByLogFile(1).size)
  }

  "insert list" should "should add 3 records" in {
    val logLineSeq: Stream[LogLineCount] = Stream.empty :+
      LogLineCount(Some(5), 1, "fake md5-value-5", 10) :+
      LogLineCount(Some(6), 1, "fake md5-value-6", 20) :+
      LogLineCount(Some(7), 1, "fake md5-value-7", 30)

    logLineCountRepo.insert(logLineSeq)
    assert(6 == logLineCountRepo.findAllLinesByLogFile(1).size)
  }

  "deleteByLogId" should "should delete all log lines" in {
    assert(3 == logLineCountRepo.deleteByLogId(1))
  }

}
