package io.cmdaa.data.dao

import com.liyaos.forklift.slick.APIMigration
import io.cmdaa.data.util.entity.{LogFileSchema, LogLikelihoodSchema, ServerSchema, UserSchema}
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import slick.migration.api.TableMigration

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class CreateDeleteTest extends FlatSpec with BeforeAndAfter {


//  val db = Database.forConfig("h2")
  val logFileRepo = new LogFileRepo(H2Profile, Database.forConfig("h2"))
  //  val db = Database.forConfig("mariadb" )
  //  val logFileRepo = new LogFileRepo(MySQLProfile, db)

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()

    val userSchema = (

      UserSchema.systems.schema ++
        UserSchema.roles.schema ++
        UserSchema.users.schema ++
        UserSchema.projects.schema ++
        UserSchema.userProjects.schema ++
        UserSchema.projectsDeployed.schema ++
        UserSchema.projectsRejected.schema ++
        UserSchema.notifications.schema ++
        UserSchema.userProjectNotifications.schema
      ).create

    DBIO.seq(

    )
    println("----------------------------------------------")
    userSchema.statements.foreach(println)
    println("----------------------------------------------")
    Await.result(db.run(DBIO.seq(userSchema).transactionally), Duration.Inf)

    val logfileSchema = (
      LogFileSchema.logSourceTypes.schema ++
        LogFileSchema.logSources.schema ++
        LogFileSchema.logFiles.schema ++
        LogFileSchema.logLines.schema
      ).create

    println("----------------------------------------------")
    logfileSchema.statements.foreach(println)
    println("----------------------------------------------")
    Await.result(db.run(DBIO.seq(logfileSchema).transactionally), Duration.Inf)

    val llSchema = LogLikelihoodSchema.logLikelihoodRun.schema.create

    println("----------------------------------------------")
    llSchema.statements.foreach(println)
    println("----------------------------------------------")
    Await.result(db.run(DBIO.seq(llSchema).transactionally), Duration.Inf)

    val serverSchema = (
      ServerSchema.servers.schema ++
        ServerSchema.labels.schema ++
        ServerSchema.serverLabels.schema
      ).create

    println("----------------------------------------------")
    serverSchema.statements.foreach(println)
    println("----------------------------------------------")
    Await.result(db.run(DBIO.seq(serverSchema).transactionally), Duration.Inf)

    session.close()
  }

  after {
    val session = db.createSession()

    val deleteServerSchema = DBIO.seq(
      ServerSchema.serverLabels.schema.drop andThen
        ServerSchema.servers.schema.drop andThen
        ServerSchema.labels.schema.drop
    )
    Await.result(db.run(deleteServerSchema.transactionally), Duration.Inf)

    val deleteLLSchema = DBIO.seq(
      LogLikelihoodSchema.logLikelihoodRun.schema.drop
    )
    Await.result(db.run(deleteLLSchema.transactionally), Duration.Inf)

    val deleteLogSchema = DBIO.seq(
      LogFileSchema.logSources.schema.drop andThen
        LogFileSchema.logSourceTypes.schema.drop andThen
        LogFileSchema.logLines.schema.drop andThen
        LogFileSchema.logFiles.schema.drop
    )
    Await.result(db.run(deleteLogSchema.transactionally), Duration.Inf)

    val deleteUserSchema = DBIO.seq(
      UserSchema.userProjects.schema.drop andThen
        UserSchema.projectsDeployed.schema.drop andThen
        UserSchema.projectsRejected.schema.drop andThen
        UserSchema.projects.schema.drop andThen
        UserSchema.users.schema.drop andThen
        UserSchema.roles.schema.drop andThen
        UserSchema.systems.schema.drop
    )
    Await.result(db.run(deleteUserSchema.transactionally), Duration.Inf)
    session.close()
  }

}


