package io.cmdaa.data.dao

import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.LogFileSchema.{LogFile, logFiles}
import io.cmdaa.data.util.entity.LogLikelihoodSchema.{LogLikeLihoodRun, logLikelihoodRun}
import io.cmdaa.data.util.entity.UserSchema._
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import scala.collection.mutable.ListBuffer
import scala.concurrent.Await
import scala.concurrent.duration.Duration


@RunWith(classOf[JUnitRunner])
class LogLikelihoodAnalyticRepoTest extends FlatSpec with BeforeAndAfter {


  val logLikelihoodRepo = new LogLikelihoodAnalyticRepo(H2Profile, Database.forConfig("h2"))

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()
    val setup = DBIO.seq(
      (systems.schema ++ users.schema ++ logLikelihoodRun.schema ++ logFiles.schema ++ projects.schema).create,
      systems += System(Some(1), "cloud 1", "This is the cloud 1 system", "modifier", 10000),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "project1", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      logFiles += LogFile(1L, "messages", "/var/log/messages", 1),
      logFiles += LogFile(2L, "fluentd", "/var/log/td-agent", 1),
      logFiles += LogFile(3L, "nginx", "/var/log/nginx", 1),
      logFiles += LogFile(4L, "apache", "/var/log/httpd", 1),

      logLikelihoodRun += LogLikeLihoodRun(1L, 0.90D, 0.05D, 1524244391L, 1L),
      logLikelihoodRun += LogLikeLihoodRun(2L, 0.90D, 0.05D, 1524244391L, 2L),
      logLikelihoodRun += LogLikeLihoodRun(3L, 0.90D, 0.05D, 1524244391L, 3L),
      logLikelihoodRun += LogLikeLihoodRun(4L, 0.90D, 0.05D, 1524244391L, 4L),

      logLikelihoodRun += LogLikeLihoodRun(5L, 0.89D, 0.05D, 1524244391L, 1L),
      logLikelihoodRun += LogLikeLihoodRun(6L, 0.89D, 0.05D, 1524158002L, 2L),
      logLikelihoodRun += LogLikeLihoodRun(7L, 0.89D, 0.05D, 1524071605L, 3L),
      logLikelihoodRun += LogLikeLihoodRun(8L, 0.89D, 0.05D, 1523985208L, 4L),

      logLikelihoodRun += LogLikeLihoodRun(9L, 0.89D, 0.07D, 1524244622L, 1L),
      logLikelihoodRun += LogLikeLihoodRun(10L, 0.89D, 0.07D, 1524158224L, 2L),
      logLikelihoodRun += LogLikeLihoodRun(11L, 0.89D, 0.07D, 1524071827L, 3L),
      logLikelihoodRun += LogLikeLihoodRun(12L, 0.89D, 0.07D, 1523985431L, 4L),

      logLikelihoodRun += LogLikeLihoodRun(13L, 0.80D, 0.05D, 1523899081L, 1L),
      logLikelihoodRun += LogLikeLihoodRun(14L, 0.80D, 0.05D, 1523812683L, 2L),
      logLikelihoodRun += LogLikeLihoodRun(15L, 0.80D, 0.05D, 1523726286L, 3L),
      logLikelihoodRun += LogLikeLihoodRun(16L, 0.80D, 0.05D, 1523639888L, 4L)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)
    session.close()
  }

  after {
    val session = db.createSession()
    val query = DBIO.seq(
      logLikelihoodRun.schema.drop andThen
        logFiles.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        systems.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)
    session.close()
  }

  "findAll" should "return all runs" in {
    assert(16 == logLikelihoodRepo.findAll().size)
  }

  "findAllWithLogFile" should "final all runs with their associated log files" in {
    assert(16 == logLikelihoodRepo.findAllWithLogFile().size)
  }

  "findAllRunsByLogFile" should "return all runs pertaining to a log" in {
    assert(4 == logLikelihoodRepo.findAllRunsByLogFile(1L).size)
  }

  "insert" should "insert a new ll run" in {
    logLikelihoodRepo.insert(LogLikeLihoodRun(17L, 0.80D, 0.05D, 1524247741L, 1L))
    assert(17 == logLikelihoodRepo.findAll().size)
  }

  "insert List" should "insert a new ll run" in {
    var recs = new ListBuffer[LogLikeLihoodRun]

    recs += LogLikeLihoodRun(17L, 0.80D, 0.05D, 1524247741L, 1L)
    recs += LogLikeLihoodRun(18L, 0.80D, 0.05D, 1524247741L, 1L)

    logLikelihoodRepo.insert(recs.toList)
    assert(18 == logLikelihoodRepo.findAll().size)
  }

  "deleteById" should "delete a record by id" in {
    assert(logLikelihoodRepo.deleteById(1L) == 1)
  }

  "deleteByLogId" should "delete all runs associated with a log and return count" in {
    assert(4 == logLikelihoodRepo.deleteByLogId(1))
  }
}
