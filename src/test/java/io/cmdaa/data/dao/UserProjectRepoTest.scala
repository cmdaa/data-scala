package io.cmdaa.data.dao

import org.junit.runner.RunWith
import io.cmdaa.data.util.entity.LogFileSchema._
import io.cmdaa.data.util.entity.UserSchema._
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class UserProjectRepoTest extends FlatSpec with BeforeAndAfter {


  val userProjectRepo = new UserProjectRepo(H2Profile, Database.forConfig("h2"))

  // userProjectRepo.connect()

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()
    val setup = DBIO.seq(
      (
        systems.schema ++
          users.schema ++
          projects.schema ++
          roles.schema ++
          userProjects.schema ++
          projectsDeployed.schema ++
          projectsRejected.schema ++
          notifications.schema ++
          userProjectNotifications.schema ++
          accessTokens.schema ++
          logSourceTypes.schema ++
          logSources.schema ++
          userApplicationProperties.schema
        ).create,

      systems += System(Some(1), "cloud 1", "This is the cloud 1 system", "modifier", 10000),
      systems += System(Some(2), "test me", "This is the system used in CRUD testing", "modifier", 10000),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "ssue", "pw_salted_hash", "Sally", "Sue", "sally.sue@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "sbob", "pw_salted_hash", "Sponge", "Bob", "spong.bob@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "ddelete", "pw_salted_hash", "Daniel", "Delete", "dan.delete@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "messages", 1, 1, suggestRegEx = true, Some(1000), Some("Messages"), "PENDING", "modifier", 10000),
      projects += Project(Some(2), "nginx", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      projects += Project(Some(3), "httpd", 1, 3, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      roles += Role(Some(1), "Super User", "This is the superuser", "modifier", 10000),
      roles += Role(Some(2), "Admin", "This is the administrator", "modifier", 10000),
      roles += Role(Some(3), "User", "This is the user", "modifier", 10000),
      roles += Role(Some(4), "Delete", "Used in delete test", "modifier", 10000),
      notifications += Notification(Some(1), "This is notification 1", 1, 10000),
      notifications += Notification(Some(2), "This is notification 2", 2, 10000),
      userProjects += UserProject(2, 1, 1),
      userProjects += UserProject(2, 3, 2),
      userProjects += UserProject(1, 2, 3),
      userProjects += UserProject(3, 2, 3),
      userProjectNotifications += UserProjectNotification(Some(1), Some(1), 1),
      userProjectNotifications += UserProjectNotification(Some(2), Some(1), 1),
      userProjectNotifications += UserProjectNotification(Some(1), None, 2),
      userProjectNotifications += UserProjectNotification(None, Some(2), 2),
      logSourceTypes += LogSourceType(Some(1), "CMDAA", "{}", "modifier", 10000)
    )
    Await.result(db.run(setup.transactionally), Duration.Inf)
    session.close()
  }

  after {
    val session = db.createSession()
    val query = DBIO.seq(
      userApplicationProperties.schema.drop andThen
        logSources.schema.drop andThen
        logSourceTypes.schema.drop andThen
        userProjects.schema.drop andThen
        projectsDeployed.schema.drop andThen
        projectsRejected.schema.drop andThen
        userProjectNotifications.schema.drop andThen
        accessTokens.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        roles.schema.drop andThen
        systems.schema.drop andThen
        notifications.schema.drop
    )
    Await.result(db.run(query.transactionally), Duration.Inf)
    session.close()
  }

  "inserting a user" should "insert a user" in {
    val user: User = userProjectRepo.insertUser(User(None, "jsmith", "pw_salted_hash", "Joe", "Smith", "jsmith@smith.net", active = true, approved = true, "modifier", 10000))
    assert(user.id.nonEmpty)
    assert(userProjectRepo.listUsers().size == 5)
  }

  "updateUser" should "update a user" in {
    val user: User = User(Some(2L), "nnewsom", "pw_salted_hash", "Newt", "Newsom", "nnewsomh@foo.net", active = true, approved = true, "modifier", 10000)
    assert(userProjectRepo.updateUser(user) == 1)
    assert(userProjectRepo.findUser(2).get.firstName == "Newt")
  }

  "updatePassword" should "update a user's password" in {
    val userId: Long = 2L
    val passwordHash = "new:salted:password:hash"
    assert(userProjectRepo.findUser(2).get.passwordHash != passwordHash)
    assert(userProjectRepo.updatePassword(userId, passwordHash) == 1)
    assert(userProjectRepo.findUser(2).get.passwordHash == passwordHash)
  }

  "deleteUser" should "delete a user" in {
    val user: User = userProjectRepo.findUser(4).get
    assert(user.id.get == 4L)
    assert(user.lastName == "Delete")
    assert(userProjectRepo.deleteUser(4) == 1)
    assert(userProjectRepo.listUsers().size == 3)
  }

  "findUser" should "find a single user" in {
    assert(userProjectRepo.findUser(2).get.id.get == 2)
  }

  "finding a user by username" should "succeed or return None" in {
    assert(userProjectRepo.findUserByUsername("jdoe").nonEmpty)
    assert(userProjectRepo.findUserByUsername("bad").isEmpty)
  }

  "listUsers" should "find 4 users" in {
    assert(userProjectRepo.listUsers().size == 4)
  }

  "insertRole" should "insert another role" in {
    userProjectRepo.insertRole(Role(None, "Guest", "this is a guest", "modifier", 10000))
    assert(userProjectRepo.listRoles().size == 5)
  }

  "updateRole" should "update a role" in {
    val role: Role = Role(Some(2L), "Guest", "this is a guest", "modifier", 10000)
    assert(userProjectRepo.updateRole(role) == 1)
    assert(userProjectRepo.findRole(2).get.name == "Guest")
  }

  "deleteRole" should "delete a role" in {
    val role: Role = userProjectRepo.findRole(4L).get
    assert(role.id.get == 4L)
    assert(role.name == "Delete")
    assert(userProjectRepo.deleteRole(4L) == 1)
    assert(userProjectRepo.listRoles().size == 3)
  }

  "findRole" should "find a single role" in {
    assert(userProjectRepo.findRole(2L).get.id.get == 2L)
  }

  "listRoles" should "find 4 roles" in {
    assert(userProjectRepo.listRoles().size == 4)
  }

  "insertSystem" should "insert another system" in {
    userProjectRepo.insertSystem(System(None, "Inserted System", "this is a new system", "modifier", 10000))
    assert(userProjectRepo.listSystems().size == 3)
  }

  "updateSystem" should "update a system" in {
    val system: System = System(Some(2L), "Update me", "this is an updated system", "modifier", 10000)
    assert(userProjectRepo.updateSystem(system) == 1)
    assert(userProjectRepo.findSystem(2).get.name == "Update me")
  }

  "deleteSystem" should "delete a system" in {
    val system: System = userProjectRepo.findSystem(2L).get
    assert(system.id.get == 2L)
    assert(system.name == "test me")
    assert(userProjectRepo.deleteSystem(2L) == 1)
    assert(userProjectRepo.listSystems().size == 1)
  }

  "findSystem" should "find a single system" in {
    assert(userProjectRepo.findSystem(2L).get.id.get == 2L)
  }

  "listSystems" should "find 2 systems" in {
    assert(userProjectRepo.listSystems().size == 2)
  }

  "findAllProjects" should "find 3 projects" in {
    assert(userProjectRepo.findAllProjects().size == 3)
  }

  "findAllUserProjects" should "find 4 users to projects" in {
    assert(userProjectRepo.findAllUserProjects().size == 4)
  }

  "addUserToProject" should "add user to project" in {
    userProjectRepo.addUserToProject(2, 2, 2)
    assert(userProjectRepo.findProjectsByUser(2).size == 3)
  }

  "addUserToProject with Case Classes" should "add user to project" in {
    userProjectRepo.addUserToProject(User(Some(2), "ssue", "pw_salted_hash", "Sally", "Sue", "sally.sue@test.com", active = true, approved = true, "modifier", 10000), Project(Some(2), "nginx", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000), Role(Some(2), "Admin", "This is the admin", "modifier", 10000))
    assert(userProjectRepo.findProjectsByUser(2).size == 3)
  }

  "finding projects by user" should "return a list of 2 projects for user 2" in {
    assert(userProjectRepo.findProjectsByUser(2).size == 2)
  }

  "listing all projects" should "return a list of 3 projects" in {
    assert(userProjectRepo.listProjects().size == 3)
  }

  "finding a project" should "find the correct project" in {
    val project: Project = userProjectRepo.findProject(1L).get
    assert(1L === project.id.get)
    assert("messages" === project.name)
    assert("modifier" === project.modifiedBy)
    assert(10000 === project.modifiedDateMillis)
    assert(1L === project.systemId)
    assert(true === project.suggestRegEx)
    assert(1000 === project.regExQuantity.get)
    assert("Messages" === project.regExUnit.get)
    assert("PENDING" === project.deployStatus)
  }

  "finding a project for a specific user" should "find the correct project" in {
    val opt: Option[(Project, UserProject)] = userProjectRepo.findProjectForUser(1L, 2L)
    assert(opt.nonEmpty)
    val tuple: (Project, UserProject) = opt.get
    assert(1L === tuple._1.id.get)
    assert("messages" === tuple._1.name)
    assert("modifier" === tuple._1.modifiedBy)
    assert(10000 === tuple._1.modifiedDateMillis)
    assert(1L === tuple._1.systemId)
    assert(1L === tuple._2.projectId)
    assert(2L === tuple._2.userId)
    assert(1L === tuple._2.roleId)
    assert(true === tuple._1.suggestRegEx)
    assert(1000 === tuple._1.regExQuantity.get)
    assert("Messages" === tuple._1.regExUnit.get)
    assert("PENDING" === tuple._1.deployStatus)
  }

  "finding all projects for a specific user" should "find all the projects for the user" in {
    assert(1 === userProjectRepo.listProjectsForUser(1L).size)
    assert(2 === userProjectRepo.listProjectsForUser(2L).size)
  }

  "finding a project for a specific owner" should "find the correct project" in {
    val opt: Option[Project] = userProjectRepo.findProjectForOwner(1L, 1L)
    assert(opt.nonEmpty)
    val project: Project = opt.get
    assert(1L === project.id.get)
    assert("messages" === project.name)
    assert("modifier" === project.modifiedBy)
    assert(10000 === project.modifiedDateMillis)
    assert(1L === project.systemId)
    assert(true === project.suggestRegEx)
    assert(1000 === project.regExQuantity.get)
    assert("Messages" === project.regExUnit.get)
    assert("PENDING" === project.deployStatus)
  }

  "finding projects by owner" should "return a list of all projects owned by the user" in {
    assert(2 === userProjectRepo.listProjectsForOwner(1).size)
    assert(1 === userProjectRepo.listProjectsForOwner(3).size)
  }

  "inserting a project" should "insert a project" in {
    val project: Project = userProjectRepo.insertProject(Project(None, "testing", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000))
    assert(project.id.nonEmpty)
    assert(userProjectRepo.listProjects().size == 4)
  }

  "inserting a project with associated log sources and users with roles " should "insert a project and associated relationships" in {
    val logSources = List((None, "one", 1L, Some(500L), "{}", None, "mod1", 10000L), (None, "two", 1L, Some(600L), "{}", None, "mod2", 20000L))
    val userRoles = List((1L, 1L), (2L, 1L))
    var project: Project = Project(None, "testing", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000)
    project = userProjectRepo.insertProject(project, logSources, userRoles)
    assert(project.id.nonEmpty)
    assert(userProjectRepo.listProjects().size == 4)
    assert(2 === userProjectRepo.listProjectsForUser(1L).size)
    assert(3 === userProjectRepo.listProjectsForUser(2L).size)
    assert(2 === userProjectRepo.listUserRolesForProject(project.id.get).size)
    val srcs = userProjectRepo.listLogSourcesForProject(project.id.get)
    assert(2 === srcs.size)
    for (s <- srcs) {
      assert(s.id.nonEmpty)
      assert(project.id.get === s.projectId)
    }
  }

  "updating a project" should "update a project" in {
    assert(1 === userProjectRepo.updateProject(Project(Some(1), "This is an updated project", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 100000)))
    val project: Project = userProjectRepo.findProject(1L).get
    assert("This is an updated project" === project.name)
    assert(100000 === project.modifiedDateMillis)
  }

  "deleting a project" should "delete the correct project" in {
    assert(1 === userProjectRepo.deleteProject(1L))
    assert(2 === userProjectRepo.listNotifications()._2.size)
  }

  "finding all users for a specified project" should "return a list of all users associated with the project" in {
    assert(1 === userProjectRepo.listUsersForProject(1L).size)
    assert(2 === userProjectRepo.listUsersForProject(2L).size)
    assert(1 === userProjectRepo.listUsersForProject(3L).size)
  }

  "finding all users/roles for a specified project" should "return a list of all users/roles associated with the project" in {
    assert(1 === userProjectRepo.listUserRolesForProject(1L).size)
    assert(2 === userProjectRepo.listUserRolesForProject(2L).size)
    assert(1 === userProjectRepo.listUserRolesForProject(3L).size)
  }

  "finding a system for a specified project" should "return the system" in {
    val system: System = userProjectRepo.getSystemForProject(3L).get
    assert(1L === system.id.get)
    assert("cloud 1" === system.name)
  }

  "finding the owner for a specified project" should "return the user" in {
    val owner: User = userProjectRepo.getOwnerForProject(3L).get
    assert(3L === owner.id.get)
    assert("sbob" === owner.username)
  }

  "deploying a project" should "deploy a project" in {
    userProjectRepo.deployProject(ProjectDeployed(1L, "TEST", "me", 10000))
    userProjectRepo.deployProject(ProjectDeployed(1L, "PRODUCTION", "me", 20000))
    assert(2 === userProjectRepo.listDeploymentsForProject(1L).size)
    assert("DEPLOYED" === userProjectRepo.findProject(1L).get.deployStatus)
  }

  "rejecting a project" should "reject a project" in {
    userProjectRepo.rejectProject(ProjectRejected(1L, "Too broad", "me", 10000))
    userProjectRepo.rejectProject(ProjectRejected(1L, "Still too broad", "me", 20000))
    assert(2 === userProjectRepo.listRejectionsForProject(1L).size)
    assert("REJECTED" === userProjectRepo.findProject(1L).get.deployStatus)
  }

  "listing all notifications" should "return a list of 2 notifications" in {
    assert(userProjectRepo.listNotifications()._2.size == 2)
  }

  "listing notifications" should "find and paginate results" in {

    userProjectRepo.insertNotification(Notification(None, "This is a notification", 1, 10000))
    userProjectRepo.insertNotification(Notification(None, "This is a notification", 1, 10000))
    userProjectRepo.insertNotification(Notification(None, "This is a notification", 1, 10000))
    userProjectRepo.insertNotification(Notification(None, "This is a notification", 1, 10000))

    var ret: (Int, List[Notification]) = userProjectRepo.listNotifications()
    assert(ret._1 === 6)
    assert(ret._2.size === 6)

    ret = userProjectRepo.listNotifications(Some(2, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 3)

    ret = userProjectRepo.listNotifications(Some(0, 12))
    assert(ret._1 === 6)
    assert(ret._2.size === 6)

    ret = userProjectRepo.listNotifications(Some(4, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 2)

    ret = userProjectRepo.listNotifications(Some(8, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 0)
  }

  "listing notifications for a user" should "find and paginate results" in {

    var ret: (Int, List[(Notification, UserProjectNotification)]) = userProjectRepo.listNotificationsForUser(1)
    assert(ret._1 === 2)
    assert(ret._2.size === 2)

    ret = userProjectRepo.listNotificationsForUser(1, Some(1, 1))
    assert(ret._1 === 2)
    assert(ret._2.size === 1)

    ret = userProjectRepo.listNotificationsForUser(1, Some(0, 12))
    assert(ret._1 === 2)
    assert(ret._2.size === 2)

    ret = userProjectRepo.listNotificationsForUser(1, Some(1, 3))
    assert(ret._1 === 2)
    assert(ret._2.size === 1)

    ret = userProjectRepo.listNotificationsForUser(1, Some(8, 3))
    assert(ret._1 === 2)
    assert(ret._2.size === 0)
  }

  "finding a notification" should "find the correct notification" in {
    val notification: Notification = userProjectRepo.findNotification(1L).get
    assert(1L === notification.id.get)
    assert("This is notification 1" === notification.message)
    assert(1 === notification.severity)
    assert(10000 === notification.issuedDateMillis)
  }

  "finding a notification for a specific user" should "find the correct notification" in {
    assert(1 === userProjectRepo.acknowledgeNotification(1L, 1L, 1L, 30000))
    val opt: Option[(Notification, UserProjectNotification)] = userProjectRepo.findNotificationForUser(1L, 1L)
    assert(opt.nonEmpty)
    val tuple: (Notification, UserProjectNotification) = opt.get
    assert(1L === tuple._1.id.get)
    assert("This is notification 1" === tuple._1.message)
    assert(1 === tuple._1.severity)
    assert(10000 === tuple._1.issuedDateMillis)
    assert(1L === tuple._2.notificationId)
    assert(1L === tuple._2.userId.get)
    assert(1L === tuple._2.projectId.get)
    assert(30000 === tuple._2.ackDateMillis.get)
  }

  "finding all notifications for a specific user" should "find all the notifications for the user" in {
    val notifications: (Int, List[(Notification, UserProjectNotification)]) = userProjectRepo.listNotificationsForUser(1L)
    assert(2 === notifications._2.size)
  }

  "inserting a notification" should "insert a notification" in {
    val notification: Notification = userProjectRepo.insertNotification(Notification(None, "This is notification 3", 2, 10000))
    assert(notification.id.nonEmpty)
    assert(userProjectRepo.listNotifications()._2.size == 3)
  }

  "inserting a notification with associated user and project " should "insert a notification and associated join" in {
    val notification: Notification = userProjectRepo.insertNotification(Notification(None, "This is notification 3", 2, 10000), List((Some(1), Some(1)), (Some(2), Some(1))))
    assert(notification.id.nonEmpty)
    assert(userProjectRepo.listNotifications()._2.size == 3)
    assert(3 === userProjectRepo.listNotificationsForUser(1L)._2.size)
    assert(2 === userProjectRepo.listNotificationsForUser(2L)._2.size)
    assert(2 === userProjectRepo.listNotificationsForProject(1L).size)
    assert(2 === userProjectRepo.listNotificationsForUserAndProject(1L, 1L).size)
  }

  "updating a notification" should "update a notification" in {
    assert(1 === userProjectRepo.updateNotification(Notification(Some(1), "This is updated notification 1", 10, 100000)))
    val notification: Notification = userProjectRepo.findNotification(1L).get
    assert("This is updated notification 1" === notification.message)
    assert(10 === notification.severity)
    assert(100000 === notification.issuedDateMillis)
  }

  "deleting a notification" should "delete the correct notification" in {
    assert(1 === userProjectRepo.deleteNotification(2L))
    assert(1 === userProjectRepo.listNotifications()._2.size)
  }

  "counting unacknowledged notifications for a user" should "work" in {
    assert(2 === userProjectRepo.countUnacknowledgedNotificationsForUser(1L))
    assert(1 === userProjectRepo.acknowledgeNotification(1L, 2L, 1000000))
    assert(1 === userProjectRepo.countUnacknowledgedNotificationsForUser(1L))
  }

  "acknowledging a notification" should "return the number of notifications acknowledged" in {
    assert(1 === userProjectRepo.acknowledgeNotification(1L, 2L, 20000))
    assert(20000 === userProjectRepo.listNotificationsAcksForUser(1L, 2L).head.ackDateMillis.get)
    assert(1 === userProjectRepo.acknowledgeNotification(1L, 1L, 1L, 30000))
    assert(30000 === userProjectRepo.listNotificationAcksForUserAndProject(1L, 1L, 1L).head.ackDateMillis.get)
  }

  "findUsersInProject" should "should return 2 projects for user" in {
    assert(userProjectRepo.findUsersInProject(2).size == 2)
  }

  "getListOfUsersWithUserType" should "do something" in {
    assert(userProjectRepo.getListOfUsersWithUserType(2).size == 2)
  }

  "access token crud operations" should "work correctly" in {
    val token1: AccessToken = userProjectRepo.generateAccessToken(1L)
    assert(token1.uuid.nonEmpty && token1.userId === 1L && token1.lastAccessDateMillis > 0)
    val token2: AccessToken = userProjectRepo.generateAccessToken(2L)
    assert(token2.uuid.nonEmpty && token2.userId === 2L && token2.lastAccessDateMillis > 0)
    assert(userProjectRepo.touchAccessToken(token2.uuid.get) === 1)
    val updatedToken2Option: Option[AccessToken] = userProjectRepo.findAccessToken(token2.uuid.get)
    assert(updatedToken2Option.nonEmpty)
    assert(updatedToken2Option.get.lastAccessDateMillis > token2.lastAccessDateMillis)
    assert(userProjectRepo.listAccessTokens().size === 2)
    assert(userProjectRepo.deleteAccessToken(token2.uuid.get) === 1)
    assert(userProjectRepo.listAccessTokens().size === 1)
    userProjectRepo.generateAccessToken(1L)
    userProjectRepo.generateAccessToken(1L)
    userProjectRepo.generateAccessToken(1L)
    assert(userProjectRepo.deleteAccessTokensForUser(1L) === 4)
  }

  "user application properties" should "work correctly" in {
    userProjectRepo.insertUserApplicationProperty(UserApplicationProperty(1L, "DEFAULT.PROJECT", "User One Project"))
    userProjectRepo.insertUserApplicationProperty(UserApplicationProperty(2L, "DEFAULT.PROJECT", "User Two Project"))
    var prop:Option[UserApplicationProperty] = userProjectRepo.findUserApplicationProperty(1L, "DEFAULT.PROJECT")
    assert(prop.nonEmpty)
    assert(1 === prop.get.userId)
    assert("DEFAULT.PROJECT" === prop.get.key)
    assert("User One Project" === prop.get.value)
    prop = userProjectRepo.findUserApplicationProperty(2L, "FOO")
    assert(prop.isEmpty)
    assert(1 === userProjectRepo.updateUserApplicationProperty(UserApplicationProperty(1L, "DEFAULT.PROJECT", "Updated User One Project")))
    prop = userProjectRepo.findUserApplicationProperty(1L, "DEFAULT.PROJECT")
    assert(prop.nonEmpty)
    assert("Updated User One Project" === prop.get.value)
    assert(2 === userProjectRepo.listUserApplicationProperties().size)
    assert(1 === userProjectRepo.listUserApplicationPropertiesForUser(1L).size)
    assert(1 === userProjectRepo.deleteUserApplicationProperty(1L, "DEFAULT.PROJECT"))
    assert(1 === userProjectRepo.listUserApplicationProperties().size)
  }
}
