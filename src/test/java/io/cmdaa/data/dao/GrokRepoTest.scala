package io.cmdaa.data.dao

import io.cmdaa.data.util.entity.GrokSchema._
import io.cmdaa.data.util.entity.LogFileSchema._
import io.cmdaa.data.util.entity.UserSchema._
import org.joda.time.DateTime
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class GrokRepoTest extends FlatSpec with BeforeAndAfter {
  val grokRepo = new GrokRepo(PostgresProfile,  Database.forConfig("h2"))
//  grokRepo.createSession()

  val db = Database.forConfig("h2")

  before {
    val session = db.createSession()
    val setup = DBIO.seq(
      (grokConfigs.schema ++ groks.schema ++ grokGrokConfigs.schema ++ grokLogMessages.schema ++ systems.schema ++ users.schema ++ logSourceTypes.schema ++ logSources.schema ++ logFiles.schema ++ projects.schema).create,
      systems += System(Some(1), "cloud 1", "This is the cloud 1 system", "modifier", 10000),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "project one", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      projects += Project(Some(2), "project two", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      logFiles += LogFile(1, "messages", "/var/log/apache", 1),
      logFiles += LogFile(2, "nginx", "/var/log/nginx", 1),
      logFiles += LogFile(3, "httpd", "/var/log/httpd", 1),
      logSourceTypes += LogSourceType(Some(1), "CMDAA", "{}", "modifier", 10000),
      logSources += LogSource(Some(1), "messages", 1, Some(500), "{}", 1, "modifier", 10000),
      logSources += LogSource(Some(2), "nginx", 1, Some(600), "{}", 1, "modifier", 10000),
      logSources += LogSource(Some(3), "httpd", 1, Some(700), "{}", 2, "modifier", 10000),
      grokConfigs += GrokConfig(Some(1), false, 1, 1, "Test User", new DateTime().toDate.getTime),
      grokConfigs += GrokConfig(Some(2), false, 1, 1, "Test User", new DateTime().toDate.getTime),
      groks += Grok(id = Some(1), parentId = -1, uuid = "d0a9a314-ec3a-4b56-855b-7165b176e23e", active = true, verified = true, grok = ".*%{LOGLEVEL:field0} %{JAVACLASS:field1}: Reading broadcast variable .*took .*ms",  logSourceId = 1),
      groks += Grok(id = Some(2), parentId = -1, uuid = "caedaed2-3421-4352-b074-2bdb7560085e", active = true, verified = true, grok = ".*%{LOGLEVEL:field0} %{JAVACLASS:field1}: Got assigned task .*",  logSourceId = 1),
      groks += Grok(id = Some(3), parentId = -1, uuid = "e2e439ec-473e-4289-8a95-91af4529265a", active = true, verified = true, grok = ".*%{LOGLEVEL:field0} %{JAVACLASS:field1}: Block .*stored as values in memory \\(estimated size .*KB, free .*KB\\)",  logSourceId = 1),
      groks += Grok(id = Some(4), parentId = -1, uuid = "d143452f-36c1-4ccf-b870-706fcb079385", active = true, verified = true, grok = "Grok 4",  logSourceId = 2),
      groks += Grok(id = Some(5), parentId = -1, uuid = "9e99bce3-9cf2-49d0-967b-fda0c5ec0840", active = true, verified = true, grok = "Grok 5",  logSourceId = 2),
      grokGrokConfigs += GrokConfigJoin(1, 1),
      grokGrokConfigs += GrokConfigJoin(1, 2),
      grokGrokConfigs += GrokConfigJoin(1, 3),
      grokGrokConfigs += GrokConfigJoin(2, 4),
      grokGrokConfigs += GrokConfigJoin(2, 5),
      grokLogMessages += GrokLogMessages(Some(1), "TEST LOG MESSAGE 1", 1, 1),
      grokLogMessages += GrokLogMessages(Some(2), "TEST LOG MESSAGE 2", 1, 1),
      grokLogMessages += GrokLogMessages(Some(3), "TEST LOG MESSAGE 3", 1, 1),
      grokLogMessages += GrokLogMessages(Some(4), "TEST LOG MESSAGE 4", 1, 1),
      grokLogMessages += GrokLogMessages(Some(5), "TEST LOG MESSAGE 5", 1, 1),
      grokLogMessages += GrokLogMessages(Some(6), "TEST LOG MESSAGE 6", 2, 2),
      grokLogMessages += GrokLogMessages(Some(7), "TEST LOG MESSAGE 7", 2, 2),
      grokLogMessages += GrokLogMessages(Some(8), "TEST LOG MESSAGE 8", 2, 2),
      grokLogMessages += GrokLogMessages(Some(9), "TEST LOG MESSAGE 9", 2, 2),
      grokLogMessages += GrokLogMessages(Some(10), "TEST LOG MESSAGE 0", 2, 2)
    )
    val result = Await.result(db.run(setup.transactionally), Duration.Inf)

    session.close()
  }

  after {
    val session = db.createSession()
    val drop = DBIO.seq(
        grokLogMessages.schema.drop andThen
        grokGrokConfigs.schema.drop andThen
        groks.schema.drop andThen
        grokConfigs.schema.drop andThen
        logSources.schema.drop andThen
        logSourceTypes.schema.drop andThen
        logFiles.schema.drop andThen
        projects.schema.drop andThen
        users.schema.drop andThen
        systems.schema.drop
    )
    Await.result(db.run(drop.transactionally), Duration.Inf)
    session.close()
  }

  "insert grok config" should "return new grok config" in {
    assert( grokRepo.insertGrokConfig( GrokConfig(None, false, 1, 1, "Test User", new DateTime().toDate.getTime) ).id.get == 3 )
  }

  "find all grok configs" should "return configs" in {
    assert( 2 == grokRepo.findAllGrokConfigs().size )
  }

  "find grok config by id" should "return the proper grok config" in {
    assert( 1 == grokRepo.findGrokConfigById(1).get.id.get )
  }


 // case class FluentGrok(path: String, grokId: String, grok: String){
 case class FluentGrok(path: String, groks: List[Grok]){
    def toXml = {
        <source>
          @type tail
          path {path}
          tag some_file_type

          <parse>
            @type grok
            { groks.sortWith( _.grok.length > _.grok.length ) map toElement }
          </parse>
        </source>
    }

   def toElement(grok: Grok):scala.xml.Elem = {
     <grok>
       name {grok.uuid}
       pattern {grok.grok}
     </grok>

   }
  }

  "find grokk config by logsource id" should "return all grok configs that are configured per log source" in {
    val  grokConfigs = grokRepo.findGrokConfigByLogSourceId(1L)

    assert(grokConfigs.size == 2)
  }

  "find grok config by logsource id doesn't exist" should "return all grok configs that are configured per log source" in {
    val  grokConfigs = grokRepo.findGrokConfigByLogSourceId(2L)

    assert(grokConfigs.size == 0)
  }

  "find groks by grok config" should "return all groks assigned to configuration" in {
    assert( 3 == grokRepo.findGroksInGrokConfig(1).length )

    val groks = grokRepo.findGroksInGrokConfig(1)



      val xml = FluentGrok("/to/do",  groks)
    val p = new scala.xml.PrettyPrinter(100, 4)
      println( "=======================\n" + p.format(xml.toXml) + "\n=======================\n"  )

    //    <source>
    //      @type tail
    //      path /path/to/log
    //      tag grokked_log
    //      <parse>
    //        @type grok
    //        grok_name_key grok_name
    //        grok_failure_key grokfailure
    //        <grok>
    //          name apache_log
    //          pattern %{COMBINEDAPACHELOG}
    //          time_format "%d/%b/%Y:%H:%M:%S %z"
    //        </grok>
    //        <grok>
    //          name ip_address
    //          pattern %{IP:ip_address}
    //        </grok>
    //        <grok>
    //          name rest_message
    //          pattern %{GREEDYDATA:message}
    //        </grok>
    //      </parse>
    //    </source>
  }

  "find all groks" should "return all runs" in {
    assert(5 == grokRepo.findAllGroks().size)
  }

  "find grok by id" should "return one" in {
    assert(grokRepo.findGrokById(1).get.id == Some(1))
    assert(grokRepo.findGrokById(2).get.id == Some(2))
    assert(grokRepo.findGrokById(3).get.id == Some(3))
    assert(grokRepo.findGrokById(4).get.id == Some(4))
    assert(grokRepo.findGrokById(5).get.id == Some(5))
  }

  "insert grok" should "insert a record" in {
    grokRepo.insertGrok(Grok(id = Some(1), parentId = -1, uuid = "6eedb83b-ce75-4c9b-a1bc-b5ff736bae94", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1))
    assert(6 == grokRepo.findAllGroks().size)
    assert(grokRepo.findGrokById(2).get.id == Some(2))
  }

  "batch insert grok" should "insert multiple records" in {
    grokRepo.insertGrok( List( Grok(id = Some(6), parentId = -1, uuid = "b0915a0b-84d2-481c-bd51-d3dcb12f998a", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1),
                               Grok(id = Some(7), parentId = -1, uuid = "a25f350e-02f1-4df4-b6ae-8b3631aee28c", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 2),
                               Grok(id = Some(8), parentId = -1, uuid = "30fd3cfd-0a3e-4ae6-8ec1-4791c70ce906", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 2) ) )
    assert(8 == grokRepo.findAllGroks().size)
    assert(grokRepo.findGrokById(6).get.id == Some(6))
    assert(grokRepo.findGrokById(7).get.id == Some(7))
    assert(grokRepo.findGrokById(8).get.id == Some(8))
  }

  "delete grok id" should "delete by the id specified" in {
    assert(grokRepo.deleteGrokByMetaDataId(2) == 2)
    assert(3 == grokRepo.findAllGroks().size)
  }

  "delete grok by meta id" should "delete by the meta id specified" in {
    assert(grokRepo.deleteGrokByMetaDataId(2) == 2)
    assert(3 == grokRepo.findAllGroks().size)
  }

  "find all groks log messages" should "return all runs" in {
    assert(10 == grokRepo.findAllGrokLogMsg().size)
  }

  "find grok log messages by id" should "return one" in {
    assert(grokRepo.findGrokLogMsgById(1).get.id.get == 1)
    assert(grokRepo.findGrokLogMsgById(2).get.id.get == 2)
    assert(grokRepo.findGrokLogMsgById(3).get.id.get == 3)
    assert(grokRepo.findGrokLogMsgById(4).get.id.get == 4)
    assert(grokRepo.findGrokLogMsgById(5).get.id.get == 5)
    assert(grokRepo.findGrokLogMsgById(6).get.id.get == 6)
    assert(grokRepo.findGrokLogMsgById(7).get.id.get == 7)
    assert(grokRepo.findGrokLogMsgById(8).get.id.get == 8)
    assert(grokRepo.findGrokLogMsgById(9).get.id.get == 9)
    assert(grokRepo.findGrokLogMsgById(10).get.id.get == 10)
  }

  "insert grok log msg" should "insert a record" in {
    grokRepo.insertGrokLogMsg(GrokLogMessages(Some(11), "TEST LOG MESSAGE 0", 1, 1))
    assert(11 == grokRepo.findAllGrokLogMsg().size)
    assert(grokRepo.findGrokLogMsgById(11).get.id.get == 11)
  }

  "batch insert grok log msg" should "insert multiple records" in {
    grokRepo.insertGrokLogMsg(List(GrokLogMessages(Some(11), "TEST LOG MESSAGE 0", 1, 1), GrokLogMessages(Some(12), "TEST LOG MESSAGE 0", 1, 1), GrokLogMessages(Some(12), "TEST LOG MESSAGE 0", 1, 1)))
    assert(13 == grokRepo.findAllGrokLogMsg().size)
    assert(grokRepo.findGrokLogMsgById(11).get.id.get == 11)
    assert(grokRepo.findGrokLogMsgById(12).get.id.get == 12)
    assert(grokRepo.findGrokLogMsgById(13).get.id.get == 13)
  }

  "insert grok and log msg" should "insert both" in {
    grokRepo.insertGrokAndLogMsgs( Grok(id = Some(6), parentId = -1, uuid = "b1cc5f04-ccf5-4ff6-8447-fd24abb7924c", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1),
                                   List( GrokLogMessages(Some(11), "TEST LOG MESSAGE 0", 1, 1),
                                         GrokLogMessages(Some(12), "TEST LOG MESSAGE 0", 1, 1),
                                         GrokLogMessages(Some(13), "TEST LOG MESSAGE 0", 1, 1)))
  }

  "insert confg grok and log msg" should "insert a config grok and log message" in {
    grokRepo.insertGrokAndLogMsgs(
      1,
       Grok(id = Some(6), parentId = -1, uuid = "b1cc5f04-ccf5-4ff6-8447-fd24abb7924c", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1),
       List( GrokLogMessages(Some(11), "TEST LOG MESSAGE 0", 1, 1),
       GrokLogMessages(Some(12), "TEST LOG MESSAGE 0", 1, 1),
       GrokLogMessages(Some(13), "TEST LOG MESSAGE 0", 1, 1))
      )


    assert(grokRepo.findGroksInGrokConfig(1).size == 4)
  }

  "delete grok log msg id" should "delete by the id specified" in {
    assert(grokRepo.deleteGrokLogMsgById(1) == 1)
    assert(9 == grokRepo.findAllGrokLogMsg().size)
  }

  "delete grok log msg by grok id" should "delete by the meta id specified" in {
    assert(grokRepo.deleteGrokLogMsgByGrokId(1) == 5)
    assert(5 == grokRepo.findAllGroks().size)
  }

  "delete groks and log msgs by logSourceId" should "delete groks and samples" in {
    assert(grokRepo.findAllGrokLogMsg().size == 10)
    grokRepo.deleteGrokById(1)
    assert(grokRepo.findAllGrokLogMsg().size == 5)
  }

  "listing log source groks" should "find and paginate results" in {
    grokRepo.insertGrok(
      List(
        Grok(id = Some(6), parentId = -1, uuid = "824dddd1-78e0-4272-b79c-0e858c82f911", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1),
        Grok(id = Some(7), parentId = -1,uuid = "66a19b8e-be8f-4fcc-a14e-d0c9392f9189", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1),
        Grok(id = Some(8), parentId = -1, uuid = "8e19c79c-7bd9-4e6a-ae43-9e9c1c374953", active = true, verified = true, grok = "Foo Bar Baz",  logSourceId = 1)
      )
    )

    var ret: (Int, List[Grok]) = grokRepo.listLogSourceGroks(1)
    assert(ret._1 === 6)
    assert(ret._2.size === 6)

    ret = grokRepo.listLogSourceGroks(1, Some(2, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 3)

    ret = grokRepo.listLogSourceGroks(1, Some(0, 12))
    assert(ret._1 === 6)
    assert(ret._2.size === 6)

    ret = grokRepo.listLogSourceGroks(1, Some(4, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 2)

    ret = grokRepo.listLogSourceGroks(1, Some(8, 3))
    assert(ret._1 === 6)
    assert(ret._2.size === 0)
  }

  "listing log source grok log messages" should "find and paginate results" in {
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 21", 1, 1))
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 22", 1, 1))
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 22", 1, 1))
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 23", 2, 1))
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 24", 2, 1))
    grokRepo.insertGrokLogMsg(GrokLogMessages(None, "TEST LOG MESSAGE 25", 2, 1))

    var ret: (Int, List[GrokLogMessages]) = grokRepo.listLogSourceGrokLogsByGrokId(1, 1)
    assert(ret._1 === 8)
    assert(ret._2.size === 8)

    ret = grokRepo.listLogSourceGrokLogsByGrokId(1, 2)
    assert(ret._1 === 3)
    assert(ret._2.size === 3)

    ret = grokRepo.listLogSourceGrokLogsByGrokId(1, 1, Some(2, 4))
    assert(ret._1 === 8)
    assert(ret._2.size === 4)

    ret = grokRepo.listLogSourceGrokLogsByGrokId(1, 1, Some(0, 12))
    assert(ret._1 === 8)
    assert(ret._2.size === 8)

    ret = grokRepo.listLogSourceGrokLogsByGrokId(1, 1, Some(6, 4))
    assert(ret._1 === 8)
    assert(ret._2.size === 2)

    ret = grokRepo.listLogSourceGrokLogsByGrokId(1, 1, Some(10, 4))
    assert(ret._1 === 8)
    assert(ret._2.size === 0)
  }

}