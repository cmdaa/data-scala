package io.cmdaa.data.migration

import io.cmdaa.data.dao.UserProjectRepo
import io.cmdaa.data.util.entity.LogFileSchema.{LogSourceType, logSourceTypes}
import io.cmdaa.data.util.entity.UserSchema._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, FlatSpec}
import slick.jdbc.JdbcBackend.Database

import slick.jdbc.H2Profile
import slick.jdbc.H2Profile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@RunWith(classOf[JUnitRunner])
class Version1Test extends FlatSpec with BeforeAndAfter {

  before{}
  after{}

  "run migration version 1" should "create the base database install for CMDAA" in {

    val version = new Version1("h2", true)
    version.runMigration()

    val userProjectRepo = new UserProjectRepo(H2Profile, Database.forConfig("h2"))
    new Notification(Some(0L), "test message", 1, 0L)

    val setup = DBIO.seq(
      systems += System(Some(1), "cloud 1", "This is the cloud 1 system", "modifier", 10000),
      systems += System(Some(2), "test me", "This is the system used in CRUD testing", "modifier", 10000),
      users += User(None, "jdoe", "pw_salted_hash", "John", "Doe", "jdoe@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "ssue", "pw_salted_hash", "Sally", "Sue", "sally.sue@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "sbob", "pw_salted_hash", "Sponge", "Bob", "spong.bob@test.com", active = true, approved = true, "modifier", 10000),
      users += User(None, "ddelete", "pw_salted_hash", "Daniel", "Delete", "dan.delete@test.com", active = true, approved = true, "modifier", 10000),
      projects += Project(Some(1), "messages", 1, 1, suggestRegEx = true, Some(1000), Some("Messages"), "PENDING", "modifier", 10000),
      projects += Project(Some(2), "nginx", 1, 1, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      projects += Project(Some(3), "httpd", 1, 3, suggestRegEx = false, None, None, "PENDING", "modifier", 10000),
      roles += Role(Some(1), "Super User", "This is the superuser", "modifier", 10000),
      roles += Role(Some(2), "Admin", "This is the administrator", "modifier", 10000),
      roles += Role(Some(3), "User", "This is the user", "modifier", 10000),
      roles += Role(Some(4), "Delete", "Used in delete test", "modifier", 10000),
      notifications += Notification(Some(1), "This is notification 1", 1, 10000),
      notifications += Notification(Some(2), "This is notification 2", 2, 10000),
      userProjects += UserProject(2, 1, 1),
      userProjects += UserProject(2, 3, 2),
      userProjects += UserProject(1, 2, 3),
      userProjects += UserProject(3, 2, 3),
      userProjectNotifications += UserProjectNotification(Some(1), Some(1), 1),
      userProjectNotifications += UserProjectNotification(Some(2), Some(1), 1),
      userProjectNotifications += UserProjectNotification(Some(1), None, 2),
      userProjectNotifications += UserProjectNotification(None, Some(2), 2),
      logSourceTypes += LogSourceType(Some(1), "CMDAA", "{}", "modifier", 10000)

    )

    val db = Database.forConfig("h2")

    Await.result(db.run(setup.transactionally), Duration.Inf)

      assert(userProjectRepo.listRoles().size == 4)

  }
}
