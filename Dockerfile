FROM openjdk:8-jre-alpine
MAINTAINER Matt D

ARG JAR_FILE
ADD target/cmdaa-data.jar /usr/share/cmdaa/cmdaa-data.jar
ADD target/classes/application.conf /usr/share/cmdaa/application.conf
ADD target/classes/logback.xml /usr/share/cmdaa/logback.xml
ADD target/classes/entrypoint.sh /usr/share/cmdaa/entrypoint.sh

#CMD ["sleep", "1000"]
#CMD ["/usr/bin/java", "-jar", "/usr/share/cmdaa/cmdaa-data.jar", "-v", "${CMDAA_DB_VERSION}", "-d", "${DB_TYPE}"]
CMD ["/bin/sh", "/usr/share/cmdaa/entrypoint.sh"]